-- CRIANDO BANCO DE DADOS

create database  BancoDeDadosTCC;
use BancoDeDadosTCC;

-- TABELAS SEM RELACIONAMENTO

create table Plano(
	id int not null primary key auto_increment,
    nome varchar(50) not null,
    velocidade int not null,
    preco double not null,
    descricao text not null,
    status boolean not null
);

create table TipoOcorrencia(
	id int not null primary key auto_increment,
    nome varchar(50) not null,
    descricao varchar(100) not null,
    tempoResolucao int not null
);

create table Setor(
	id int not null primary key auto_increment,
    nome varchar(50) not null,
    descricao text not null
);

create table Etapa(
	id int not null primary key auto_increment,
    nome varchar(50) not null,
    descricao text not null
);

create table TipoCliente(
	id int not null primary key auto_increment,
    nome varchar(50) not null,
    descricao text not null,
    nivelUrgencia int not null
);

-- TABELAS COM RELACIONAMENTO FK

create table Cargo(
	id int not null primary key auto_increment,
    nome varchar(50) not null,
    descricao text not null,
    setor_id int not null,
    foreign key (setor_id) references Setor(id)
);

create table Cliente(
	id int not null primary key auto_increment,
    nomeCompleto varchar(40) not null,
    dataNascimento date not null,
    cpfcnpj varchar(20) not null,
    email varchar(40) not null,
    telefone varchar(20) not null,
    cidade varchar(40) not null,
    estado varchar(20) not null,
    rua varchar(60) not null,
    bairro varchar(40) not null,
    complemento text not null,
    cep varchar(40) not null,
    tipoCliente_id int not null,
    foreign key (tipoCliente_id) references TipoCliente(id),
    status boolean not null  
);

create table Contrato(
	id int not null primary key auto_increment,
    dataContratacao datetime not null,
    diaVencimento int not null,
    contrato bit,
    cliente_id int not null,
    plano_id int not null,
    foreign key (cliente_id) references Cliente(id),
    foreign key (plano_id) references Plano(id)
);

create table ConexaoStatus(
	id int not null primary key auto_increment,
    onOff boolean not null,
    pagamento date not null,
    reducaoVelocidade boolean not null,
    statusContrato boolean not null,
    contrato_id int not null,
    foreign key (contrato_id) references Contrato(id)
);

create table Funcionario(
	id int not null primary key auto_increment,
    nomeCompleto varchar(40) not null,
    dataNascimento date not null,
    cpfcnpj varchar(20) not null,
    email varchar(40) not null,
    telefone varchar(20) not null,
    cidade varchar(40) not null,
    estado varchar(20) not null,
    rua varchar(60) not null,
    bairro varchar(40) not null,
    complemento text not null,
    cep varchar(40) not null,
    salario double not null,
    dataAdmissao date not null,
    dataDemissao date,
    ultimoAcesso datetime not null,
    usuario varchar(20) not null,
    password text not null,
    cargo_id int not null,
    setor_id int not null,
    foreign key (cargo_id) references Cargo(id),
    foreign key (setor_id) references Setor(id)
);

create table AtendimentoOcorrencia(
	id int not null primary key auto_increment,
    descricaoOcorrencia text not null,
    dataAbertura datetime not null,
    dataLimite datetime not null,
    dataConclusao datetime, 
    tipoOcorrencia_id int not null,
    contrato_id int not null,
    foreign key (tipoOcorrencia_id) references TipoOcorrencia(id),
    foreign key (contrato_id) references Contrato(id)
);

insert into TipoCliente (nome, descricao, nivelUrgencia) values ('Pessoa Fisica', 'Baixa urgencia', 1);
insert into TipoCliente (nome, descricao, nivelUrgencia) values ('Comercial', 'Media urgencia', 2);
insert into TipoCliente (nome, descricao, nivelUrgencia) values ('Empresarial', 'Alta urgencia', 3);

-- TABELAS APENAS DE RELACIONAMENTOS