<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Etapa;
use App\Models\Menu;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EtapaController extends Controller {
    
    //Listagem das Etapas
    public function indexView() {
        try {
            if (Auth::user()->nivelAcesso == 1) {
                $menus = Menu::where('nivelAcesso', 1)->get();
            } else if(Auth::user()->nivelAcesso == 2) {
                $menus = Menu::whereBetween('nivelAcesso', [1, 2])->get();
            } else {
                $menus = Menu::all();
            }
    
            $menuLinks = $menus->pluck('link');
    
            if ($menuLinks->contains('/etapas')) {
                return view('etapas.index', ['menus' => $menus]);
            } else {
                return redirect()->back();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function index($tipo = null, $conteudo = null) {
        try {
            if (isset($conteudo) && isset($tipo)) {
                return Etapa::where("$tipo", 'LIKE', "%$conteudo%")->get();
            } else {
                return Etapa::orderBy("nome")->get();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    //Cadastro de nova Etapa
    public function store(Request $request) {
        try {
            DB::beginTransaction();

            $novaEtapa = new Etapa($request->all());
            $novaEtapa->save();

            DB::commit();
            return $novaEtapa->toJson();
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'message' => 'Verifique os dados e tente novamente.',
            ], 500);
        }
    }

    //Mostra dados de Etapa
    public function show($id) {
        try {
            $ShowEtapa = Etapa::find($id);
            if (isset($ShowEtapa)) {
                return json_encode($ShowEtapa);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    //Atualiza uma Etapa
    public function update(Request $request, $id) {
        try {
            DB::beginTransaction();

            $UpdateEtapa = Etapa::find($id);

            $UpdateEtapa->update($request->all());
            $UpdateEtapa->save();

            DB::commit();
            return json_encode($UpdateEtapa);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'message' => 'Verifique os dados e tente novamente.',
            ], 500);
        }
    }

    //Deleta uma Etapa
    public function destroy($id) {
        try {
            DB::beginTransaction();

            $DeleteEtapa = Etapa::findOrFail($id);
            if (isset($DeleteEtapa)) {
                $DeleteEtapa->delete();
            }

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
        
    }
}
