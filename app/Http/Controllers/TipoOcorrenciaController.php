<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;
use App\Models\TipoOcorrencia;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TipoOcorrenciaController extends Controller {
    
    //Listagem dos Tipos de Ocorrencia
    public function indexView() {
        try {
            if (Auth::user()->nivelAcesso == 1) {
                $menus = Menu::where('nivelAcesso', 1)->get();
            } else if(Auth::user()->nivelAcesso == 2) {
                $menus = Menu::whereBetween('nivelAcesso', [1, 2])->get();
            } else {
                $menus = Menu::all();
            }
    
            $menuLinks = $menus->pluck('link');
    
            if ($menuLinks->contains('/tiposOcorrencias')) {
                return view('tiposOcorrencias.index', ['menus' => $menus]);
            } else {
                return redirect()->back();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function index($tipo = null, $conteudo = null) {
        try {
            if (isset($conteudo) && isset($tipo)) {
                return TipoOcorrencia::where("$tipo", 'LIKE', "%$conteudo%")->get();
            } else {
                return TipoOcorrencia::orderBy("nome")->get();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    //Cadastro de novo Tipo de Ocorrencia 
    public function store(Request $request) {
        try {
            DB::beginTransaction();

            $novoTipoOcorrencia = new TipoOcorrencia($request->all());
            $novoTipoOcorrencia->save();

            DB::commit();
            return json_encode($novoTipoOcorrencia);

        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'message' => 'Verifique os dados e tente novamente.',
            ], 500);
        }
    }

    //Mostra dados de Tipo de Ocorrencia
    public function show($id) {
        try {
            $ShowTipoOcorrencia = TipoOcorrencia::find($id);
            if (isset($ShowTipoOcorrencia)) {
                return json_encode($ShowTipoOcorrencia);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    //Atualiza um Tipo de Ocorrencia
    public function update(Request $request, $id) {
        try {
            DB::beginTransaction();

            $UpdateTipoOcorrencia = TipoOcorrencia::find($id);
            $UpdateTipoOcorrencia->update($request->all());

            DB::commit();
            return json_encode($UpdateTipoOcorrencia);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'message' => 'Verifique os dados e tente novamente.',
            ], 500);
        }
    }

    //Deleta um Tipo de Ocorrencia
    public function destroy($id) {
        try {
            DB::beginTransaction();

            $DeleteTipoOcorrencia = TipoOcorrencia::find($id);
            if (isset($DeleteTipoOcorrencia)) {
                $DeleteTipoOcorrencia->delete();
            }

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
        
    }
}
