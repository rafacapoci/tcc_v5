<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;
use App\Models\Plano;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PlanoController extends Controller {
    
    //Listagem dos Planos
    public function indexView() {
        try {
            if (Auth::user()->nivelAcesso == 1) {
                $menus = Menu::where('nivelAcesso', 1)->get();
            } else if(Auth::user()->nivelAcesso == 2) {
                $menus = Menu::whereBetween('nivelAcesso', [1, 2])->get();
            } else {
                $menus = Menu::all();
            }
    
            $menuLinks = $menus->pluck('link');
    
            if ($menuLinks->contains('/planos')) {
                return view('planos.index', ['menus' => $menus]); 
            } else {
                return redirect()->back();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function index($tipo = null, $conteudo = null){
        try {
            if (isset($conteudo) && isset($tipo)) {
                return Plano::where("$tipo", 'LIKE', "%$conteudo%")->get();
            } else {
                return Plano::orderBy("nome")->get();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    //Cadastro de Planos 
    public function store(Request $request) {
        try {
            DB::beginTransaction();

            $request['preco'] = preg_replace("/[^0-9 ]/", '', $request['preco']);
            $novoPlano = new Plano($request->all());
            $novoPlano->save();

            DB::commit();
            return json_encode($novoPlano);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'message' => 'Verifique os dados e tente novamente.',
            ], 500);
        }
    }

    //Mostra dados de Plano
    public function show($id) {
        try {
            $ShowPlano = Plano::find($id);
            if (isset($ShowPlano)) {
                return json_encode($ShowPlano);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    //Atualiza um Tipo de Ocorrencia
    public function update(Request $request, $id) {
        try {
            DB::beginTransaction();

            $request['preco'] = preg_replace("/[^0-9 ]/", '', $request['preco']);
            $UpdatePlano = Plano::find($id);
            $UpdatePlano->update($request->all());

            DB::commit();
            return json_encode($UpdatePlano);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'message' => 'Verifique os dados e tente novamente.',
            ], 500);
        }
    }

    //Deleta um Tipo de Ocorrencia
    public function destroy($id) {
        try {
            DB::beginTransaction();

            $DeletePlano = Plano::find($id);
            if (isset($DeletePlano)) {
                $DeletePlano->delete();
            }

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
        
    }
}