<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use App\Models\Contrato;
use App\Models\Endereco;
use App\Models\Menu;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ContratoController extends Controller {
    
    //Listagem dos Clientes
    public function indexView() {
        try {
            if (Auth::user()->nivelAcesso == 1) {
                $menus = Menu::where('nivelAcesso', 1)->get();
            } else if(Auth::user()->nivelAcesso == 2) {
                $menus = Menu::whereBetween('nivelAcesso', [1, 2])->get();
            } else {
                $menus = Menu::all();
            }
    
            $menuLinks = $menus->pluck('link');
    
            if ($menuLinks->contains('/contratos')) {
                return view('contratos.index', ['menus' => $menus]);
            } else {
                return redirect()->back();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function index($tipo = null, $conteudo = null) {
        try {
            if (isset($conteudo) && isset($tipo)) {
                return Contrato::with('cliente', 'plano', 'endereco')->where("$tipo", 'LIKE', "$conteudo")->get();
            } else {
                return Contrato::with('cliente', 'plano', 'endereco')->orderBy("dataContratacao")->get();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    //Cadastro de novo Cliente
    public function store(Request $request) {
        try {
            DB::beginTransaction();

            $novoEndereco = new Endereco();
            $novoEndereco->cidade = $request->cidade;
            $novoEndereco->estado = $request->estado;
            $novoEndereco->rua = $request->rua;
            $novoEndereco->bairro = $request->bairro;
            $novoEndereco->complemento = $request->complemento;
            $novoEndereco->cep = $request->cep;
            $novoEndereco->save();

            $novoContrato = new Contrato();
            $novoContrato->cliente_id = $request->cliente_id;
            $novoContrato->plano_id = $request->plano_id;
            $novoContrato->endereco_id = $novoEndereco->id;
            $novoContrato->dataContratacao = $request->dataContratacao;
            $novoContrato->diaVencimento = $request->diaVencimento;
            $novoContrato->save();

            $contrato = Contrato::with('cliente', 'plano', 'endereco')->find($novoContrato->id);

            DB::commit();
            return json_encode($contrato);  
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'message' => 'Verifique os dados e tente novamente.',
            ], 500);
        }
    }

    //Mostra dados de Cliente
    public function show($id) {
        try {
            $ShowContrato = Contrato::with('cliente', 'plano', 'endereco')->find($id);
            if (isset($ShowContrato)) {
                return json_encode($ShowContrato);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    //Atualiza um Cliente
    public function update(Request $request, $id) {
        try {
            DB::beginTransaction();

            $updateContrato = Contrato::find($id);
            $updateContrato->cliente_id = $request->cliente_id;
            $updateContrato->plano_id = $request->plano_id;
            $updateContrato->dataContratacao = $request->dataContratacao;
            $updateContrato->diaVencimento = $request->diaVencimento;
            $updateContrato->update();
            
            $updateEndereco = Endereco::find($updateContrato->endereco_id);
            $updateEndereco->cidade = $request->cidade;
            $updateEndereco->estado = $request->estado;
            $updateEndereco->rua = $request->rua;
            $updateEndereco->bairro = $request->bairro;
            $updateEndereco->complemento = $request->complemento;
            $updateEndereco->cep = $request->cep;
            $updateEndereco->update();

            $UpdateContrato = Contrato::with('cliente', 'plano', 'endereco')->find($updateContrato->id);
            
            DB::commit();
            return json_encode($UpdateContrato);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'message' => 'Verifique os dados e tente novamente.',
            ], 500);
        }
    }

    //Deleta um Cliente
    public function destroy($id) {
        try {
            DB::beginTransaction();

            $DeleteContrato = Contrato::find($id);
            if (isset($DeleteContrato)) {
                $DeleteContrato->delete();
            }

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
        
    }
}
