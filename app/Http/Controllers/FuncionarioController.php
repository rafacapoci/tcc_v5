<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Funcionario;
use App\Models\Menu;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class FuncionarioController extends Controller {
    
    //Listagem dos Funcionarios
    public function indexView() {
        try {
            if (Auth::user()->nivelAcesso == 1) {
                $menus = Menu::where('nivelAcesso', 1)->get();
            } else if(Auth::user()->nivelAcesso == 2) {
                $menus = Menu::whereBetween('nivelAcesso', [1, 2])->get();
            } else {
                $menus = Menu::all();
            } 
    
            $menuLinks = $menus->pluck('link');
    
            if ($menuLinks->contains('/funcionarios')) {
                return view('funcionarios.index', ['menus' => $menus]);
            } else {
                return redirect()->back();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function index($tipo = null, $conteudo = null){
        try {
            if (isset($conteudo) && isset($tipo)) {
                return Funcionario::with('setor', 'cargo')->where("$tipo", 'LIKE', "%$conteudo%")->get();
            } else {
                return Funcionario::with('setor', 'cargo')->orderBy("nomeCompleto")->get();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
 
    //Cadastro de novo funcionarios
    public function store(Request $request) {
        try {
            $request->validate([
                'cpfcnpj' => 'unique:Funcionario',
                'email' => 'unique:Funcionario'
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'Um funcionario com este e-mail e/ou cpf já está cadastrado.',
            ], 500);
        }

        try {
            DB::beginTransaction();

            if ((date("Y") - date('Y', strtotime($request['dataNascimento']))) > 90) {
                return response()->json([
                    'message' => 'Data de nascimento invalida.',
                ], 500);
            }
            
            $request['salario'] = preg_replace("/[^0-9 ]/", '', $request['salario']);

            $novoFuncionario = new Funcionario($request->all());
            $novoFuncionario->password = Hash::make($request['password']);
            $novoFuncionario->ultimoAcesso = now();
            $novoFuncionario->save();

            $funcionario = Funcionario::with('setor', 'cargo')->find($novoFuncionario->id);

            DB::commit();
            return json_encode($funcionario);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'message' => 'Verifique os dados e tente novamente.',
            ], 500);
        }
    }

    //Mostra dados de Funcionarios
    public function show($id){
        try {
            $ShowFuncionario = Funcionario::with('setor', 'cargo')->find($id);
        
            if (isset($ShowFuncionario)) {
                return json_encode($ShowFuncionario);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    //Atualiza um Funcionario
    public function update(Request $request, $id) {
        try {
            DB::beginTransaction();

            if ((date("Y") - date('Y', strtotime($request['dataNascimento']))) > 90) {
                return response()->json([
                    'message' => 'Data de nascimento invalida.',
                ], 500);
            }

            $request['salario'] = preg_replace("/[^0-9 ]/", '', $request['salario']);

            $UpdateFuncionario = Funcionario::find($id);

            if ($request['email'] != $UpdateFuncionario->email) {
                try {
                    $request->validate([
                        'email' => 'unique:Funcionario'
                    ]);
                } catch (\Throwable $th) {
                    return response()->json([
                        'message' => 'Um funcionario com este e-mail e/ou cpf já está cadastrado.',
                    ], 500);
                }
            }

            if ($request['cpfcnpj'] != $UpdateFuncionario->cpfcnpj) {
                try {
                    $request->validate([
                        'cpfcnpj' => 'unique:Funcionario'
                    ]);
                } catch (\Throwable $th) {
                    return response()->json([
                        'message' => 'Um funcionario com este e-mail e/ou cpf já está cadastrado.',
                    ], 500);
                }
            }

            $UpdateFuncionario->update($request->all());
            $UpdateFuncionario->password = Hash::make($request->input('password'));

            $UpdateFuncionario->save(); 

            $UpdateFuncionario = Funcionario::with('setor', 'cargo')->find($UpdateFuncionario->id);

            DB::commit();
            return json_encode($UpdateFuncionario);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'message' => 'Verifique os dados e tente novamente.',
            ], 500);
        }
    }

    //Deleta um Funcionario
    public function destroy($id){
        try {
            DB::beginTransaction();

            $DeleteFuncionario = Funcionario::find($id);
            if (isset($DeleteFuncionario)) {
                $DeleteFuncionario->delete();
            }

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }
}