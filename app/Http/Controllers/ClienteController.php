<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use App\Models\Cliente;
use App\Models\Menu;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class ClienteController extends Controller {
    
    //Listagem dos Clientes
    public function indexView() {
        try {
            if (Auth::user()->nivelAcesso == 1) {
                $menus = Menu::where('nivelAcesso', 1)->get();
            } else if(Auth::user()->nivelAcesso == 2) {
                $menus = Menu::whereBetween('nivelAcesso', [1, 2])->get();
            } else {
                $menus = Menu::all();
            }
    
            $menuLinks = $menus->pluck('link');
    
            if ($menuLinks->contains('/clientes')) {
                return view('clientes.index', ['menus' => $menus]);
            } else {
                return redirect()->back();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function index($tipo = null, $conteudo = null) {
        try {
            if (isset($conteudo) && isset($tipo)) {
                return Cliente::with('tipoCliente', 'contratos.endereco')->where("$tipo", 'LIKE', "%$conteudo%")->get();
            } else {
                return Cliente::with('tipoCliente')->orderBy("nomeCompleto")->get();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    //Cadastro de novo Cliente
    public function store(Request $request) {
        try {
            $request->validate([
                'cpfcnpj' => 'unique:Cliente',
                'email' => 'unique:Cliente'
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'Uma conta com este e-mail e/ou cpf já existe.',
            ], 500);
        }

        try {
            DB::beginTransaction();

            if ((date("Y") - date('Y', strtotime($request['dataNascimento']))) > 90) {
                return response()->json([
                    'message' => 'Data de nascimento invalida.',
                ], 500);
            }

            $novoCliente = new Cliente($request->all());
            $novoCliente->save();
            $cliente = Cliente::with('tipoCliente')->find($novoCliente->id);
            
            DB::commit();
            return json_encode($cliente);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'message' => 'Verifique os dados e tente novamente.',
            ], 500);
        }
    }

    //Mostra dados de Cliente
    public function show($id) {
        try {
            $ShowCliente = Cliente::with('tipoCliente', 'contratos.plano', 'contratos.endereco')->find($id);
            if (isset($ShowCliente)) {
                return json_encode($ShowCliente);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    //Atualiza um Cliente
    public function update(Request $request, $id) { 
        try {
            DB::beginTransaction();

            if ((date("Y") - date('Y', strtotime($request['dataNascimento']))) > 90) {
                return response()->json([
                    'message' => 'Data de nascimento invalida.',
                ], 500);
            }

            $UpdateCliente = Cliente::find($id);

            if ($request['email'] != $UpdateCliente->email) {
                try {
                    $request->validate([
                        'email' => 'unique:Cliente'
                    ]);
                } catch (\Throwable $th) {
                    return response()->json([
                        'message' => 'Uma conta com este e-mail e/ou cpf já existe.',
                    ], 500);
                }
            }

            if ($request['cpfcnpj'] != $UpdateCliente->cpfcnpj) {
                try {
                    $request->validate([
                        'cpfcnpj' => 'unique:Cliente'
                    ]);
                } catch (\Throwable $th) {
                    return response()->json([
                        'message' => 'Uma conta com este e-mail e/ou cpf já existe.',
                    ], 500);
                }
            }

            $UpdateCliente->update($request->all());
            $UpdateCliente = Cliente::with('tipoCliente')->find($id);

            DB::commit();
            return json_encode($UpdateCliente);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'message' => 'Verifique os dados e tente novamente.',
            ], 500);
        }
    }

    //Deleta um Cliente
    public function destroy($id) {
        try {
            DB::beginTransaction();

            $DeleteCliente = Cliente::find($id);
            if (isset($DeleteCliente)) {
                $DeleteCliente->delete();
            }

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }
}
