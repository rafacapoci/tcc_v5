<?php

namespace App\Http\Controllers;

use App\Models\TipoCliente;

class TipoClienteController extends Controller {
    
    public function index() {
        try {
            return TipoCliente::all()->toJson();
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
