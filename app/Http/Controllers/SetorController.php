<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;
use App\Models\Setor;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SetorController extends Controller {
    
    //Listagem dos Setores
    public function indexView() {
        try {
            if (Auth::user()->nivelAcesso == 1) {
                $menus = Menu::where('nivelAcesso', 1)->get();
            } else if(Auth::user()->nivelAcesso == 2) {
                $menus = Menu::whereBetween('nivelAcesso', [1, 2])->get();
            } else {
                $menus = Menu::all();
            }
    
            $menuLinks = $menus->pluck('link');
    
            if ($menuLinks->contains('/setores')) {
                return view('setores.index', ['menus' => $menus]);
            } else {
                return redirect()->back();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    } 

    public function index($tipo = null, $conteudo = null) {
        try {
            if (isset($conteudo) && isset($tipo)) {
                return Setor::where("$tipo", 'LIKE', "%$conteudo%")->get();
            } else {
                return Setor::orderBy("nome")->get();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    //Cadastro de novo Setor
    public function store(Request $request) {
        try {
            DB::beginTransaction();

            $novoSetor = new Setor($request->all());
            $novoSetor->save();
            
            DB::commit();
            return json_encode($novoSetor);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'message' => 'Verifique os dados e tente novamente.',
            ], 500);
        }
    }

    //Mostra dados de Setor
    public function show($id) {
        try {
            $ShowSetor = Setor::find($id);
            if (isset($ShowSetor)) {
                return json_encode($ShowSetor);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    //Atualiza um Setor
    public function update(Request $request, $id) {
        try {
            DB::beginTransaction();

            $UpdateSetor = Setor::find($id);
            $UpdateSetor->update($request->all());

            DB::commit();
            return json_encode($UpdateSetor);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'message' => 'Verifique os dados e tente novamente.',
            ], 500);
        }
    }

    //Deleta um Setor
    public function destroy($id) {
        try {
            DB::beginTransaction();

            $DeleteSetor = Setor::find($id);
            if (isset($DeleteSetor)) {
                $DeleteSetor->delete();
            }

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }
}
