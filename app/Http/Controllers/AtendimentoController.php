<?php

namespace App\Http\Controllers;

use App\Models\AtendimentoOcorrencia;
use App\Models\Etapa_Has_AtendimentoOcorrencia;
use App\Models\Menu;
use App\Models\TipoOcorrencia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AtendimentoController extends Controller {

    public function indexView() {
        try {
            if (Auth::user()->nivelAcesso == 1) {
                $menus = Menu::where('nivelAcesso', 1)->get();
            } else if(Auth::user()->nivelAcesso == 2) {
                $menus = Menu::whereBetween('nivelAcesso', [1, 2])->get();
            } else {
                $menus = Menu::all();
            }
    
            $menuLinks = $menus->pluck('link');
    
            if ($menuLinks->contains('/home')) {
                return view('atendimentos.index', ['menus' => $menus]);
            } else {
                return redirect()->back();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function index($tipo = null, $conteudo = null){
        try {
            if (isset($conteudo) && isset($tipo)) {
                return AtendimentoOcorrencia::with('etapas_has_atendimentosOcorrencias.etapa', 'contrato.cliente', 'tipoOcorrencia')->where("$tipo", 'LIKE', "%$conteudo%")->get();
            } else {
                return AtendimentoOcorrencia::with('etapas_has_atendimentosOcorrencias.etapa', 'contrato.cliente', 'tipoOcorrencia')->orderBy("dataAbertura")->get();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function store(Request $request) {
        try {
            DB::beginTransaction();

            $tipoOcorrencia = TipoOcorrencia::find($request->tipoOcorrencia_id);

            $novoAtendimento = new AtendimentoOcorrencia();
            $novoAtendimento->descricaoOcorrencia = $request->descricaoOcorrencia;
            $novoAtendimento->tipoOcorrencia_id = $request->tipoOcorrencia_id;
            $novoAtendimento->contrato_id = $request->contrato_id;
            $novoAtendimento->dataAbertura = date('Y-m-d h:m:s');
            $novoAtendimento->dataLimite = date('Y-m-d h:m:s', strtotime('+' . $tipoOcorrencia->tempoResolucao . ' hours'));
            $novoAtendimento->save();

            $novaEtapaHasAtendimento = new Etapa_Has_AtendimentoOcorrencia();
            $novaEtapaHasAtendimento->descricaoEtapa = $request->descricaoOcorrencia;
            $novaEtapaHasAtendimento->dataEtapa = date('Y-m-d h:m:s');
            $novaEtapaHasAtendimento->etapa_id = $request->etapa_id;
            $novaEtapaHasAtendimento->atendimentoOcorrencia_id = $novoAtendimento->id;
            $novaEtapaHasAtendimento->save();

            $novoAtendimento = AtendimentoOcorrencia::with('etapas_has_atendimentosOcorrencias', 'contrato.cliente', 'tipoOcorrencia')->find($novoAtendimento->id);

            DB::commit();
            return json_encode($novoAtendimento);
        } catch (\Throwable $th) {
            DB::rollBack();
            return $th;
        }
    }
}