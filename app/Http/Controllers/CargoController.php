<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cargo;
use App\Models\Menu;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CargoController extends Controller {

    //Listagem dos Cargos
    public function indexView() {
        try {
            if (Auth::user()->nivelAcesso == 1) {
                $menus = Menu::where('nivelAcesso', 1)->get();
            } else if(Auth::user()->nivelAcesso == 2) {
                $menus = Menu::whereBetween('nivelAcesso', [1, 2])->get();
            } else {
                $menus = Menu::all();
            }
    
            $menuLinks = $menus->pluck('link');
    
            if ($menuLinks->contains('/cargos')) {
                return view('cargos.index', ['menus' => $menus]);
            } else {
                return redirect()->back();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function index($tipo = null, $conteudo = null){
        try {
            if (isset($conteudo) && isset($tipo)) {
                return Cargo::with('setor')->where("$tipo", 'LIKE', "%$conteudo%")->get();
            } else {
                return Cargo::with('setor')->orderBy("nome")->get();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    //Cadastro de novo Cargo
    public function store(Request $request) {
        try {
            DB::beginTransaction();

            $novoCargo = new Cargo($request->all());
            $novoCargo->save();

            $cargo = Cargo::with('setor')->find($novoCargo->id);

            DB::commit();
            return json_encode($cargo);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'message' => 'Verifique os dados e tente novamente.',
            ], 500);
        }
    }

    //Mostra dados de Cargo
    public function show($id) {
        try {
            $ShowCargo = Cargo::find($id);
            if (isset($ShowCargo)) {
                return json_encode($ShowCargo);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    //Atualiza um Cargo
    public function update(Request $request, $id) {
        try {
            DB::beginTransaction();

            $UpdateCargo = Cargo::find($id);
            $UpdateCargo->update($request->all());
            $UpdateCargo->save();

            $UpdateCargo = Cargo::with('setor')->find($UpdateCargo->id);

            DB::commit();
            return json_encode($UpdateCargo);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'message' => 'Verifique os dados e tente novamente.',
            ], 500);
        }
    }

    //Deleta um Cargo
    public function destroy($id) {
        try {
            DB::beginTransaction();

            $DeleteCargo = Cargo::find($id);
            if (isset($DeleteCargo)) {
                $DeleteCargo->delete();
            }

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }
}
