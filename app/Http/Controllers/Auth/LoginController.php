<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller {
    
    use AuthenticatesUsers;

    //Para onde redirecionar após o login
    protected $redirectTo = RouteServiceProvider::HOME;

    //Cria nova instancia do controller
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }
}