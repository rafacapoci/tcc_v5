<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setor extends Model {

    protected $fillable = ['nome', 'descricao'];
    
    protected $table = 'Setor';
    public $timestamps = false;

    public function funcionarios() {
        return $this->hasMany(Funcionario::class);
    }

    public function cargos() {
        return $this->hasMany(Cargo::class);
    }
}