<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Etapa_Has_AtendimentoOcorrencia extends Model {
    
    protected $fillable = [
        'descricaoEtapa', 
        'dataEtapa', 
        'etapa_id',
        'atendimentoOcorrencia_id'
    ];
    
    protected $table = 'Etapa_Has_AtendimentoOcorrencia';
    public $timestamps = false;

    public function etapa(){
        return $this->belongsTo(Etapa::class, 'etapa_id', 'id');
    }

    public function atendimentoOcorrencia(){
        return $this->belongsTo(AtendimentoOcorrencia::class, 'atendimentoOcorrencia_id', 'id');
    }
}