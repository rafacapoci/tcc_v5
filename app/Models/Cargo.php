<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cargo extends Model {

    protected $fillable = ['nome', 'descricao', 'setor_id'];
    protected $table = 'Cargo';
    public $timestamps = false;

    public function funcionarios() {
        return $this->hasMany(Funcionario::class);
    }

    public function setor(){
        return $this->belongsTo(Setor::class, 'setor_id', 'id');
    }
}