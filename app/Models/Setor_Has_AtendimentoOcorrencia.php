<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setor_Has_AtendimentoOcorrencia extends Model {

    protected $fillable = [
        'dataResgate', 
        'dataLargou', 
        'setor_id',
        'atendimentoOcorrencia_id',
        'funcionario_id'
    ];
    protected $table = 'Setor_Has_AtendimentoOcorrencia';
    public $timestamps = false;

    public function setor(){
        return $this->belongsTo(Setor::class, 'setor_id', 'id');
    }

    public function atendimentoOcorrencia(){
        return $this->belongsTo(AtendimentoOcorrencia::class, 'atendimentoOcorrencia_id', 'id');
    }

    public function funcionario(){
        return $this->belongsTo(Funcionario::class, 'funcionario_id', 'id');
    }

    
}