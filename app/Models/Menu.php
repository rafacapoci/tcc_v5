<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model {

    protected $fillable = ['nome', 'link', 'nivelAcesso'];
    protected $table = 'Menus';
    public $timestamps = false;

}