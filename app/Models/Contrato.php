<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contrato extends Model
{

    protected $fillable = [
        'cliente_id',
        'plano_id',
        'endereco_id', 
        'dataContratacao', 
        'diaVencimento'
    ];

    protected $table = 'Contrato';
    public $timestamps = false;
    use HasFactory;

    public function plano() {
        return $this->belongsTo(Plano::class, 'plano_id', 'id');
    }

    public function endereco() {
        return $this->belongsTo(Endereco::class, 'endereco_id', 'id');
    }

    public function cliente() {
        return $this->belongsTo(Cliente::class, 'cliente_id', 'id');
    }

}
