<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model {

    protected $fillable = [
        'nomeCompleto',
        'dataNascimento',
        'cpfcnpj',
        'email',
        'telefone',
        'cidade',
        'estado',
        'rua',
        'bairro',
        'complemento',
        'cep',
        'tipoCliente_id',
        'status'
    ];

    protected $table = 'Cliente';
    public $timestamps = false;
    use HasFactory; 
    
    //Tem
    public function contratos() {
        return $this->hasMany(Contrato::class);
    }

    //Pertence
    public function tipoCliente() {
        return $this->belongsTo(TipoCliente::class, 'tipoCliente_id', 'id');
    }

}
