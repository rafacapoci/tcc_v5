<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plano extends Model {

    protected $fillable = ['nome', 'velocidade', 'preco', 'descricao', 'status'];
    
    protected $table = 'Plano';
    public $timestamps = false;

    //Tem
    public function contratos() {
        return $this->hasMany(Contrato::class);
    }
}
