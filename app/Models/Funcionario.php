<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Funcionario extends Authenticatable {

    protected $fillable = [
        'nomeCompleto', 
        'dataNascimento', 
        'cpfcnpj', 
        'email', 
        'telefone', 
        'cidade', 
        'estado', 
        'rua', 
        'bairro', 
        'complemento', 
        'cep', 
        'salario', 
        'dataAdmissao' , 
        'dataDemissao', 
        'ultimoAcesso', 
        'password', 
        'nivelAcesso', 
        'setor_id', 
        'cargo_id'
    ];

    protected $table = 'Funcionario';
    public $timestamps = false;
    use HasFactory, Notifiable;

    //Pertence a
    public function cargo() {
        return $this->belongsTo(Cargo::class, 'cargo_id', 'id');
    }
 
    //Pertence a
    public function setor() {
        return $this->belongsTo(Setor::class, 'setor_id', 'id');
    }
}
