<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AtendimentoOcorrencia extends Model {

    protected $fillable = [
        'descricaoOcorrencia', 
        'dataAbertura', 
        'dataLimite',
        'dataConclusao',
        'tipoOcorrencia_id',
        'contrato_id'
    ];
    protected $table = 'AtendimentoOcorrencia';
    public $timestamps = false;

    public function etapas_has_atendimentosOcorrencias(){
        return $this->hasMany(Etapa_Has_AtendimentoOcorrencia::class, 'atendimentoOcorrencia_id', 'id');
    }

    public function contrato(){
        return $this->belongsTo(Contrato::class, 'contrato_id', 'id');
    }

    public function tipoOcorrencia(){
        return $this->belongsTo(TipoOcorrencia::class, 'tipoOcorrencia_id', 'id');
    }
}