<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model {

    protected $fillable = [
        'cidade', 
        'estado', 
        'rua', 
        'bairro', 
        'complemento', 
        'cep'
    ];
    protected $table = 'Endereco';
    public $timestamps = false;

    public function contrato() {
        return $this->hasOne(Contrato::class);
    }
}
