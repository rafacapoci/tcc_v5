<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Etapa extends Model {

    protected $fillable = ['nome', 'descricao'];
    protected $table = 'Etapa';
    public $timestamps = false;
}
