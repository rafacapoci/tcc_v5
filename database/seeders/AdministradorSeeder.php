<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdministradorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Funcionario')->insert([
            [
                'nomeCompleto' => 'Rafael Capoci',
                'dataNascimento' => '1997-02-25',
                'cpfcnpj' => '092.320.089-47',
                'email' => 'rafaelcapoci@gmail.com',
                'telefone' => '(44)9 8401-7849',
                'cidade' => 'Alto Piquiri',
                'estado' => 'Pr',
                'rua' => 'Santos Dumond',
                'bairro' => 'Centro',
                'complemento' => 'Casa de esquina',
                'cep' => '87580-000',
                'salario' => '2000',
                'dataAdmissao' => '2022-01-20',
                'dataDemissao' => null,
                'ultimoAcesso' => now(),
                'password' => Hash::make('12345678'),
                'cargo_id' => 1,
                'setor_id' => 1,
                'nivelAcesso' => 3
            ]
        ]);
    }
}
