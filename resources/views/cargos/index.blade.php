@extends('layouts.principal')
<!--Titulo-->
@section('titulo', 'Cargos')
<!--Titulo Listagem-->
@section('tituloLista', 'Cargos Cadastrados')
<!--Menus do nivel de acesso-->
@section('menus')
    @foreach ($menus as $menu)
        <a type="button" class="btn btn-primary m-1 text-light" href="{{$menu->link}}">{{$menu->nome}}</a>
    @endforeach
@endsection
<!-- Tipos de pesquisa-->
@section('tiposPesquisa')
    <option value="nome">Nome</option>
    <option value="descricao">Descriçao</option>
@endsection
<!--Inicio Tabela de Listagem-->
@section('idTabela', 'cargos')
@section('cabecalhoLista')
    <th>Nome</th>
    <th>Descrição</th>
    <th>Setor</th> 
    <th class="col-lg-2">Ações</th> 
@endsection
@section('corpoLista')
    @section('getpostjson', 'cargos')
    '<tr>' +
        '<td hidden>' + cargos[i].id + '</td>' +
        '<td>' + cargos[i].nome + '</td>' +
        '<td>' + cargos[i].descricao + '</td>' +
        '<td>' + cargos[i].setor.nome +'</td>' +
        '<td class="text-center">' + 
            '<button type="button" class="btn btn-primary mr-1" onclick="editar('+ cargos[i].id +')">Editar</button>' +
            '<button type="button" class="btn btn-danger" onclick="modalExcluir('+ cargos[i].id +')">Excluir</button>' +
        '</td>' +
    '</tr>'
@endsection

<!--Botoes Modal Adicionar Cargo-->
@section('btnAdd', 'Adicionar Cargo')
@section('tituloModal', 'Novo Cargo')
<!--Configurações Botao Salvar Dentro do Modal-->
@section('tipoBtn', 'submit')
@section('nomeBtn', 'Salvar')
<!--Formulario Modal Adicionar Cargo-->
@section('formularioAdicionar')
    <input hidden type="text" class="form-control" name="id" id="id">
    <div class="form-group">
        <label for="nome">Nome</label>
        <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome do novo cargo">
    </div>
    <div class="form-group">
        <label for="velocidade">Descrição</label>
        <textarea name="descricao" id="descricao" class="form-control" cols="10" rows="6" placeholder="Descrição sobre novo cargo"></textarea>
    </div>
    <div class="form-group">
        <label for="setor">Setor</label>
        <select class="form-control" name="setor_id" id="setor_id">
            <script>
                $(function() {
                   $.getJSON('/api/setores', function(data){
                        for (i = 0; i < data.length; i++) {
                            $('#setor_id').append('<option value="' + data[i].id + '">' + data[i].nome + '</option>');
                        }
                   }) 
                });
            </script>
        </select>
    </div>
@endsection
<!--Javascript Adicionar Cargo-->
@section('nomeNovoItem', 'novoCargo')
@section('conteudoNovoItem')
    nome: $('#nome').val(),
    descricao: $('#descricao').val(),
    setor_id: $('#setor_id').val()
@endsection
<!--Javascript Atualizar Etapa-->
@section('atualizarTabela')

    '<tr>' +
        '<td hidden>' + novoItem.id + '</td>' +
        '<td>' + novoItem.nome + '</td>' +
        '<td>' + novoItem.descricao + '</td>' +
        '<td>' + novoItem.setor.nome + '</td>' +
        '<td class="text-center">' + 
            '<button type="button" class="btn btn-primary mr-1" onclick="editar('+ novoItem.id +')">Editar</button>' +
            '<button type="button" class="btn btn-danger" onclick="modalExcluir('+ novoItem.id +')">Excluir</button>' +
        '</td>' +
    '</tr>'

@endsection
<!--Javascript Zerar inputs-->
@section('zerarInputs')
    $('#id').val(''),
    $('#nome').val(''),
    $('#descricao').val('')
    $('#setor_id').val('')
@endsection
<!--Javascript Editar Etapa-->
@section('carregarDadosEditar')
    $('#id').val(data.id),
    $('#nome').val(data.nome),
    $('#descricao').val(data.descricao),
    $('#setor_id').val(data.setor_id),
    $('#modalAdd').modal('show')
@endsection
@section('linhaAtualizar')
    linhaAtualizar[0].cells[1].textContent = atualizarItem.nome;
    linhaAtualizar[0].cells[2].textContent = atualizarItem.descricao;
    linhaAtualizar[0].cells[3].textContent = atualizarItem.setor.nome;
@endsection

@section('js')
    <script>
        $(function () {
            $('#formularioAdd').validate({
                errorElement: "em",
                validClass: "success",
                errorClass: "invalid text-danger small border-danger",
                rules: {
                    nome: {
                        required: true  
                    },
                    descricao: {
                        required: true  
                    },
                    setor_id: {
                        required: true
                    }
                },
                messages: {
                    nome: {
                        required: "Nome é obrigatório"
                    },
                    descricao: {
                        required: "Descrição é obrigatório"  
                    },
                    setor_id: {
                        required: "Setor é obrigatório"  
                    }
                }
            })
        });
    </script>
@endsection