@extends('layouts.principal')
<!--Titulo-->
@section('titulo', 'Tipos de Ocorrencia')
<!--Titulo Listagem-->
@section('tituloLista', 'Tipos de Ocorrencias Cadastradas')
<!--Menus do nivel de acesso-->
@section('menus')
    @foreach ($menus as $menu)
        <a type="button" class="btn btn-primary m-1 text-light" href="{{$menu->link}}">{{$menu->nome}}</a>
    @endforeach
@endsection
<!-- Tipos de pesquisa-->
@section('tiposPesquisa')
    <option value="nome">Nome</option>
    <option value="descricao">Descriçao</option>
    <option value="tempoResolucao">Tempo Resolução</option>
@endsection
<!--Inicio Tabela de Listagem-->
@section('idTabela', 'tabelaTipoOcorrencia')
@section('cabecalhoLista')
    <th scope="col">Nome</th>
    <th scope="col">Descrição</th>
    <th scope="col">Tempo/Horas</th>
    <th class="col-lg-2" scope="col">Ações</th>
@endsection
@section('corpoLista')
    @section('getpostjson', 'tiposOcorrencias')

    '<tr>' +
        '<td hidden>' + tiposOcorrencias[i].id + '</td>' +
        '<td>' + tiposOcorrencias[i].nome + '</td>' +
        '<td>' + tiposOcorrencias[i].descricao + '</td>' +
        '<td>' + tiposOcorrencias[i].tempoResolucao + '</td>' +
        '<td class="text-center">' + 
            '<button type="button" class="btn btn-primary mr-1" onclick="editar('+ tiposOcorrencias[i].id +')">Editar</button>' +
            '<button type="button" class="btn btn-danger" onclick="modalExcluir('+ tiposOcorrencias[i].id +')">Excluir</button>' +
        '</td>' +
    '</tr>'
        
@endsection
<!--Botoes Modal Adicionar Tipo Ocorrencias-->
@section('btnAdd', 'Adicionar tipo de Ocorrencia')
@section('tituloModal', 'Novo Tipo de Ocorrencia')
<!--Configurações Botao Salvar Dentro do Modal-->
@section('tipoBtn', 'submit')
@section('nomeBtn', 'Salvar')
<!--Formulario Modal Adicionar Tipo Ocorrencia-->
@section('formularioAdicionar')

    <input hidden type="text" class="form-control" name="id" id="id">
    <div class="form-group">
        <label for="nome">Nome</label>
        <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome da nova ocorrencia">
    </div>

    <div class="form-group">
        <label for="descricao">Descrição</label>
        <textarea name="descricao" id="descricao" class="form-control" cols="10" rows="6" placeholder="Descrição sobre nova ocorrencia"></textarea>
    </div>

    <div class="form-group">
        <label for="tempoResolucao">Tempo de Resolução</label>
        <select name="tempoResolucao" id="tempoResolucao" class="form-control">
            <option value=""></option>
            <option value="5">05 horas</option>
            <option value="8">08 horas</option>
            <option value="24"> 1 dia</option>
            <option value="48">2 dias</option>
        </select>
    </div>

@endsection
<!--Javascript Adicionar Tipo Ocorrencia-->
@section('nomeNovoItem', 'novoTipoOcorrencia')
@section('conteudoNovoItem')

    nome: $('#nome').val(),
    descricao: $('#descricao').val(),
    tempoResolucao: $('#tempoResolucao').val()

@endsection
<!--Javascript Atualizar Tipo Ocorrencia-->
@section('atualizarTabela')

    '<tr>' +
        '<td hidden>' + novoItem.id + '</td>' +
        '<td>' + novoItem.nome + '</td>' +
        '<td>' + novoItem.descricao + '</td>' +
        '<td>' + novoItem.tempoResolucao + '</td>' +
        '<td class="text-center">' + 
            '<button type="button" class="btn btn-primary mr-1" onclick="editar('+ novoItem.id +')">Editar</button>' +
            '<button type="button" class="btn btn-danger" onclick="modalExcluir('+ novoItem.id +')">Excluir</button>' +
        '</td>' +
    '</tr>'

@endsection
<!--Javascript Zerar inputs-->
@section('zerarInputs')
    $('#id').val(''),
    $('#nome').val(''),
    $('#descricao').val(''),
    $('#tempoResolucao').val('')
@endsection
<!--Javascript Editar Tipo Ocorrencia-->
@section('carregarDadosEditar')
    $('#id').val(data.id),
    $('#nome').val(data.nome),
    $('#descricao').val(data.descricao),
    $('#tempoResolucao').val(data.tempoResolucao)
    $('#modalAdd').modal('show')
@endsection
@section('linhaAtualizar')
    linhaAtualizar[0].cells[1].textContent = atualizarItem.nome;
    linhaAtualizar[0].cells[2].textContent = atualizarItem.descricao;
    linhaAtualizar[0].cells[3].textContent = atualizarItem.tempoResolucao;
@endsection

@section('js')
    <script>
        $(function () {
            $('#formularioAdd').validate({
                errorElement: "em",
                validClass: "success",
                errorClass: "invalid text-danger small border-danger",
                rules: {
                    nome: {
                        required: true  
                    },
                    descricao: {
                        required: true  
                    },
                    tempoResolucao: {
                        required: true  
                    }
                },
                messages: {
                    nome: {
                        required: "Nome é obrigatório"
                    },
                    descricao: {
                        required: "Descrição é obrigatório"  
                    },
                    tempoResolucao: {
                        required: "Tempo para resolução é obrigatório"  
                    },
                }
            })
        });
    </script>
@endsection