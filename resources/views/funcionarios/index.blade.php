@extends('layouts.principal')
<!--Titulo-->
@section('titulo', 'Funcionarios')
<!--Titulo Listagem-->
@section('tituloLista', 'Funcionarios Cadastrados')
<!--Menus do nivel de acesso-->
@section('menus')
    @foreach ($menus as $menu)
        <a type="button" class="btn btn-primary m-1 text-light" href="{{$menu->link}}">{{$menu->nome}}</a>
    @endforeach
@endsection
<!-- Tipos de pesquisa-->
@section('tiposPesquisa')
    <option value="nomeCompleto">Nome</option>
    <option value="cpfcnpj">Documento</option>
    <option value="email">E-mail</option>
    <option value="telefone">Celular</option>
@endsection
<!--Inicio Tabela de Listagem-->
@section('idTabela', 'funcionarios')
@section('cabecalhoLista')
    <th scope="col">Nome</th>
    <th scope="col">Setor</th>
    <th scope="col">Função</th>
    <th class="col-lg-2" scope="col">Ações</th>
@endsection
@section('corpoLista')
    @section('getpostjson', 'funcionarios')
    '<tr>' +
        '<td hidden>' + funcionarios[i].id + '</td>' +
        '<td>' + funcionarios[i].nomeCompleto + '</td>' +
        '<td>' + funcionarios[i].setor.nome + '</td>' +
        '<td>' + funcionarios[i].cargo.nome + '</td>' +
        '<td class="text-center">' + 
            '<button type="button" class="btn btn-primary mr-1" onclick="editar('+ funcionarios[i].id +')">Editar</button>' +
            '<button type="button" class="btn btn-danger" onclick="modalExcluir('+ funcionarios[i].id +')">Excluir</button>' +
        '</td>' +
    '</tr>'
@endsection 
<!--Botoes Modal Adicionar Funcionario-->
@section('btnAdd', 'Adicionar Funcionario')
@section('tituloModal', 'Novo Funcionario')
<!--Configurações Botao Salvar Dentro do Modal-->
@section('tipoBtn', 'submit')
@section('nomeBtn', 'Salvar')
<!--Formulario Modal Adicionar Funcionario-->
@section('formularioAdicionar')
    <div class="h4 m-4 text-center">Dados pessoais</div>
    <input hidden type="text" class="form-control" name="id" id="id">
    <div class="form-group">
        <label for="nome">Nome completo</label>
        <input type="text" class="form-control required" name="nome" id="nome" placeholder="Nome completo do funcionario">
    </div>
    <div class="form-group">
        <label for="dataNasc">Data de Nascimento</label>
        <input type="date" class="form-control" name="dataNasc" id="dataNasc">
    </div>
    <div class="form-group">
        <label for="cpfcnpj">CPF/CNPJ</label>
        <input type="text" class="form-control" name="cpfcnpj" id="cpfcnpj" placeholder="CPF ou CNPJ do funcionario">
    </div>
    <div class="form-group">
        <label for="email">E-mail</label>
        <input type="email" class="form-control" name="email" id="email" placeholder="Um ou mais emails do funcionario">
    </div>
    <div class="form-group">
        <label for="senha">Senha para Login</label>
        <input type="password" class="form-control required" name="password" id="password" placeholder="Senha do funcionario">
    </div>
    <div class="form-group">
        <label for="nivelAcesso">Nivel de Acesso</label>
        <select class="form-control" id="nivelAcesso">
            <option value="1">Limitado</option>
            <option value="2">Intermediario</option>
            <option value="3">Administrador</option>
        </select>
    </div>
    <div class="form-group">
        <label for="celular">Celular</label>
        <input type="text" class="form-control" name="celular" id="celular" placeholder="Um ou mais celulares do funcionario">
    </div>

    <div class="h4 m-4 text-center">Outros dados</div>
    <div class="form-group">
        <label for="dataAdmissao">Contratado em</label>
        <input type="date" class="form-control required" name="dataAdmissao" id="dataAdmissao">
    </div>
    <div class="form-group">
        <label for="salario">Salario</label>
        <input type="text" class="form-control required" name="salario" id="salario" placeholder="Salario do funcionario">
    </div>
    <div class="form-group">
        <label for="setor">Setor</label>
        <select class="form-control" name="setor" id="setor_id">
            <script>
                $(function() {
                   $.getJSON('/api/setores', function(data){
                        for (i = 0; i < data.length; i++) {
                            $('#setor_id').append('<option value="' + data[i].id + '">' + data[i].nome + '</option>');
                        }
                   }) 
                });
            </script>
        </select>
    </div>
    <div class="form-group">
        <label for="cargo">Cargo</label>
        <select class="form-control" name="cargo" id="cargo_id">
            <script>
                $(function() {
                   $.getJSON('/api/cargos', function(data){
                        for (i = 0; i < data.length; i++) {
                            $('#cargo_id').append('<option value="' + data[i].id + '">' + data[i].nome + '</option>');
                        }
                   }) 
                });
            </script>
        </select>
    </div>
    <div class="h4 m-4 text-center">Endereço</div>
    
    <div class="form-group">
        <label for="cep">CEP</label>
        <input type="text" class="form-control" name="cep" id="cep" placeholder="CEP do endereço do cliente">
    </div>
    <div class="row">
        <div class="form-group col-9">
            <label for="cidade">Cidade</label>
            <input disabled type="text" class="form-control" name="cidade" id="cidade" placeholder="Cidade do cliente">
        </div>
        <div class="form-group col-3">
            <label for="estado">Estado</label>
            <input disabled type="text" class="form-control" name="estado" id="estado" placeholder="Estado">
        </div>
    </div>
    <div class="form-group">
        <label for="rua">Rua</label>
        <input type="text" class="form-control" name="rua" id="rua" placeholder="Rua do cliente">
    </div>
    <div class="form-group">
        <label for="bairro">Bairro</label>
        <input type="text" class="form-control" name="bairro" id="bairro" placeholder="Bairro do cliente">
    </div>
    <div class="form-group">
        <label for="complemento">Complemento</label>
        <input type="text" class="form-control" name="complemento" id="complemento" placeholder="Complemento do cliente">
    </div>

@endsection
<!--Javascript Adicionar Funcionario-->
@section('nomeNovoItem', 'novoFuncionario')
@section('conteudoNovoItem')
    nomeCompleto: $('#nome').val(),
    dataNascimento: $('#dataNasc').val(),   
    cpfcnpj: $('#cpfcnpj').val(),
    email: $('#email').val(),
    telefone: $('#celular').val(),
    dataAdmissao: $('#dataAdmissao').val(),
    salario: $('#salario').val(),
    usuario: $('#usuario').val(),
    password: $('#password').val(),
    nivelAcesso: $('#nivelAcesso').val(),
    setor_id: $('#setor_id').val(),
    cargo_id: $('#cargo_id').val(),
    cidade: $('#cidade').val(),
    estado: $('#estado').val(),
    rua: $('#rua').val(),
    bairro: $('#bairro').val(),
    complemento: $('#complemento').val(),
    cep: $('#cep').val()
@endsection
<!--Javascript Atualizar Lista Funcionario-->
@section('atualizarTabela')
    '<tr>' +
        '<td hidden>' + novoItem.id + '</td>' +
        '<td>' + novoItem.nomeCompleto + '</td>' +
        '<td>' + novoItem.setor.nome + '</td>' +
        '<td>' + novoItem.cargo.nome + '</td>' +
        '<td class="text-center">' + 
            '<button type="button" class="btn btn-primary mr-1" onclick="editar('+ novoItem.id +')">Editar</button>' +
            '<button type="button" class="btn btn-danger" onclick="modalExcluir('+ novoItem.id +')">Excluir</button>' +
        '</td>' +
    '</tr>'
@endsection
<!--Javascript Zerar inputs-->
@section('zerarInputs')
    $('#id').val(''),
    $('#nome').val(''),
    $('#dataNasc').val(''),
    $('#cpfcnpj').val(''),
    $('#email').val(''),
    $('#celular').val(''),
    $('#dataAdmissao').val(''),
    $('#salario').val(''),
    $('#usuario').val(''),
    $('#password').val(''),
    $('#setor').val(''),
    $('#cargo').val(''),
    $('#cidade').val(''),
    $('#estado').val(''),
    $('#rua').val(''),
    $('#bairro').val(''),
    $('#complemento').val(''),
    $('#cep').val('')
@endsection
<!--Javascript Editar Funcionario-->
@section('carregarDadosEditar')
    $('#id').val(data.id),
    $('#nome').val(data.nomeCompleto),
    $('#dataNasc').val(data.dataNascimento),
    $('#cpfcnpj').val(data.cpfcnpj),
    $('#email').val(data.email),
    $('#celular').val(data.telefone),
    $('#dataAdmissao').val(data.dataAdmissao),
    $('#salario').val(data.salario),
    $('#usuario').val(data.usuario),
    $('#password').val(data.password),
    $('#nivelAcesso').val(data.nivelAcesso),
    $('#cargo').val(data.cargo),
    $('#setor').val(data.setor),
    $('#cidade').val(data.cidade),
    $('#estado').val(data.estado),
    $('#rua').val(data.rua),
    $('#bairro').val(data.bairro),
    $('#complemento').val(data.complemento),
    $('#cep').val(data.cep),
    $('#modalAdd').modal('show')
@endsection
@section('linhaAtualizar')
    linhaAtualizar[0].cells[1].textContent = atualizarItem.nomeCompleto;
    linhaAtualizar[0].cells[2].textContent = atualizarItem.setor.nome;
    linhaAtualizar[0].cells[3].textContent = atualizarItem.cargo.nome;
@endsection

@section('js')
    <script>
        $(function () {

            $('#cpfcnpj').inputmask({
                mask: ["999.999.999-99", "99.999.999/9999-99",],
                placeholder: "",
            });

            $('#celular').inputmask({
                mask: ["9999-9999", "(99) 9 9999-9999", ],
                placeholder: "",
            });

            $('#salario').inputmask('R$9{1,10}');

            $('#cep').inputmask('99999-999');

        })

        $('#cep').focusout(function(){
            $.getJSON("https://viacep.com.br/ws/"+ $('#cep').val() +"/json/?callback=?", function(dados) {
                if (!("erro" in dados)) {
                    console.log(dados);

                    $("#cidade").val(dados.localidade);
                    $("#estado").val(dados.uf);
                    $("#rua").val(dados.logradouro);
                    $("#bairro").val(dados.bairro);

                }
                else {

                    $('#cep').val("")
                    alert("CEP não encontrado.");

                }
            });
        });

    </script>

    <script>
        $(function () {
            $('#formularioAdd').validate({
                errorElement: "em",
                validClass: "success",
                errorClass: "invalid text-danger small border-danger",
                rules: {
                    nome: {
                        required: true  
                    },
                    dataNasc: {
                        required: true  
                    },
                    cpfcnpj: {
                        required: true,
                        minlength: 14
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    celular: {
                        required: true  
                    },
                    dataAdmissao: {
                        required: true  
                    },
                    salario: {
                        required: true  
                    },
                    usuario: {
                        required: true  
                    },
                    password: {
                        required: true,
                        minlength: 8 
                    },
                    nivelAcesso: {
                        required: true,
                    },
                    setor_id: {
                        required: true  
                    },
                    cargo_id: {
                        required: true  
                    },
                    cidade: {
                        required: true  
                    },
                    estado: {
                        required: true  
                    },
                    rua: {
                        required: true  
                    },
                    bairro: {
                        required: true  
                    },
                    complemento: {
                        required: true  
                    },
                    cep: {
                        required: true  
                    }
                },
                messages: {
                    nome: {
                        required: "Nome é obrigatório"
                    },
                    
                    dataNasc: {
                        required: "Data de nascimento é obrigatório"  
                    },
                    cpfcnpj: {
                        required: "CPF ou CNPJ é obrigatório",
                        minlength: jQuery.validator.format("Digite um documento valido")
                    },
                    email: {
                        required: "E-mail é obrigatório",
                        email: "E-mail invalido"
                    },
                    celular: {
                        required: "Celular é obrigatório"  
                    },
                    dataAdmissao: {
                        required: "Data de admissão é obrigatório"  
                    },
                    salario: {
                        required: "Salario é obrigatório"  
                    },
                    password: {
                        required: "Senha é obrigatório",
                        minlength: jQuery.validator.format("A senha precisa ter 8 digitos")
                    },
                    nivelAcesso: {
                        required: "Nivel de acesso é obrigatório"  
                    },
                    setor: {
                        required: "Setor é obrigatório"  
                    },
                    cargo: {
                        required: "Cargo é obrigatório"  
                    },
                    cidade: {
                        required: "Cidade é obrigatório"  
                    },
                    estado: {
                        required: "Estado é obrigatório"  
                    },
                    rua: {
                        required: "Rua é obrigatório"  
                    },
                    bairro: {
                        required: "Bairro é obrigatório"  
                    },
                    complemento: {
                        required: "Complemento é obrigatório"  
                    },
                    cep: {
                        required: "CEP é obrigatório"  
                    }
                }
            })
        });
    </script>
@endsection