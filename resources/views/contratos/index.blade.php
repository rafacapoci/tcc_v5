@extends('layouts.principal')
<!--Titulo-->
@section('titulo', 'Contratos')
<!--Titulo Listagem-->
@section('tituloLista', 'Contratos Cadastrados')
<!--Menus do nivel de acesso-->
@section('menus')
    @foreach ($menus as $menu)
        <a type="button" class="btn btn-primary m-1 text-light" href="{{$menu->link}}">{{$menu->nome}}</a>
    @endforeach
@endsection
<!-- Tipos de pesquisa-->
@section('tiposPesquisa')
    <option value="cliente->nomeCompleto">Cliente</option>
    <option value="nome">Plano</option>
@endsection
<!--Inicio Tabela de Listagem-->
@section('idTabela', 'contratos')
@section('cabecalhoLista')
    <th>Cliente</th>
    <th>Plano</th>
    <th>Contratado</th>
    <th>Vencimento</th> 
    <th class="col-lg-2">Ações</th> 
@endsection
@section('corpoLista')
    @section('getpostjson', 'contratos')
    '<tr>' +
        '<td hidden>' + contratos[i].id + '</td>' +
        '<td>' + contratos[i].cliente.nomeCompleto + '</td>' +
        '<td>' + contratos[i].plano.nome + '</td>' +
        '<td>' + contratos[i].dataContratacao + '</td>' +
        '<td>Dia ' + contratos[i].diaVencimento +'</td>' +
        '<td class="text-center">' + 
            '<button type="button" class="btn btn-primary mr-1" onclick="editar('+ contratos[i].id +')">Editar</button>' +
            '<button type="button" class="btn btn-danger" onclick="modalExcluir('+ contratos[i].id +')">Excluir</button>' +
        '</td>' +
    '</tr>'
@endsection

<!--Botoes Modal Adicionar Cargo-->
@section('btnAdd', 'Criar Contrato')
@section('tituloModal', 'Novo Contrato')
<!--Configurações Botao Salvar Dentro do Modal-->
@section('tipoBtn', 'submit')
@section('nomeBtn', 'Salvar')
<!--Formulario Modal Adicionar Cargo-->
@section('formularioAdicionar')
    <div class="h4 m-4 text-center">Dados de Contrato</div>
    <input hidden type="text" class="form-control" name="id" id="id">
    <div class="form-group">
        <label for="cliente">Cliente</label>
        <select class="form-control" name="cliente_id" id="cliente_id">
            <script>
                $(function() {
                   $.getJSON('/api/clientes', function(data){
                        for (i = 0; i < data.length; i++) {
                            $('#cliente_id').append('<option value="' + data[i].id + '">' + data[i].nomeCompleto + '</option>');
                        }
                   }) 
                });
            </script>
        </select>
    </div>

    <div class="form-group">
        <label for="plano">Plano</label>
        <select class="form-control" name="plano_id" id="plano_id">
            <script>
                $(function() {
                   $.getJSON('/api/planos', function(data){
                        for (i = 0; i < data.length; i++) {
                            $('#plano_id').append('<option value="' + data[i].id + '">' + data[i].nome + '</option>');
                        }
                   }) 
                });
            </script>
        </select>
    </div>

    <div class="form-group">
        <label for="data_contratacao">Data de contratação</label>
        <input type="date" class="form-control required" name="data_contratacao" id="data_contratacao">
    </div>

    <div class="form-group">
        <label for="data_vencimento">Dia do Vencimento</label>
        <input type="text" class="form-control" name="data_vencimento" id="data_vencimento" placeholder="Dia em que a cobrança será realizada">
    </div>


    <div class="h4 mt-4 text-center">Endereço</div>
    <div class="form-group">
        <label for="cep">CEP</label>
        <input type="text" class="form-control" name="cep" id="cep" placeholder="CEP do endereço do cliente">
    </div>
    <div class="row">
        <div class="form-group col-9">
            <label for="cidade">Cidade</label>
            <input disabled type="text" class="form-control" name="cidade" id="cidade" placeholder="Cidade do cliente">
        </div>
        <div class="form-group col-3">
            <label for="estado">Estado</label>
            <input disabled type="text" class="form-control" name="estado" id="estado" placeholder="Estado">
        </div>
    </div>
    <div class="form-group">
        <label for="rua">Rua</label>
        <input type="text" class="form-control" name="rua" id="rua" placeholder="Rua do cliente">
    </div>
    <div class="form-group">
        <label for="bairro">Bairro</label>
        <input type="text" class="form-control" name="bairro" id="bairro" placeholder="Bairro do cliente">
    </div>
    <div class="form-group">
        <label for="complemento">Complemento</label>
        <input type="text" class="form-control" name="complemento" id="complemento" placeholder="Complemento do cliente">
    </div>
    
@endsection
<!--Javascript Adicionar Cargo-->
@section('nomeNovoItem', 'novoContrato')
@section('conteudoNovoItem')
    cliente_id: $('#cliente_id').val(),
    plano_id: $('#plano_id').val(),
    dataContratacao: $('#data_contratacao').val(),
    diaVencimento: $('#data_vencimento').val(),
    contrato: $('#arquivo_contrato').val(),
    cidade: $('#cidade').val(),
    estado: $('#estado').val(),
    rua: $('#rua').val(),
    bairro: $('#bairro').val(),
    complemento: $('#complemento').val(),
    cep: $('#cep').val()
@endsection
<!--Javascript Atualizar Etapa-->
@section('atualizarTabela')

    '<tr>' +
        '<td hidden>' + novoItem.id + '</td>' +
        '<td>' + novoItem.cliente.nomeCompleto + '</td>' +
        '<td>' + novoItem.plano.nome + '</td>' +
        '<td>' + novoItem.dataContratacao + '</td>' +
        '<td>' + novoItem.diaVencimento + '</td>' +
        '<td class="text-center">' + 
            '<button type="button" class="btn btn-primary mr-1" onclick="editar('+ novoItem.id +')">Editar</button>' +
            '<button type="button" class="btn btn-danger" onclick="modalExcluir('+ novoItem.id +')">Excluir</button>' +
        '</td>' +
    '</tr>'

@endsection
<!--Javascript Zerar inputs-->
@section('zerarInputs')
    $('#id').val(''),
    $('#cliente_id').val(''),
    $('#plano_id').val(''),
    $('#data_contratacao').val(''),
    $('#data_vencimento').val(''),
    $('#arquivo_contrato').val(''),
    $('#cidade').val(''),
    $('#estado').val(''),
    $('#rua').val(''),
    $('#bairro').val(''),
    $('#complemento').val(''),
    $('#cep').val('')
@endsection
<!--Javascript Editar Etapa-->
@section('carregarDadosEditar')
    $('#id').val(data.id),
    $('#cliente_id').val(data.cliente_id),
    $('#plano_id').val(data.plano_id),
    $('#data_contratacao').val(data.dataContratacao),
    $('#data_vencimento').val(data.diaVencimento),
    $('#cidade').val(data.endereco.cidade),
    $('#estado').val(data.endereco.estado),
    $('#rua').val(data.endereco.rua),
    $('#bairro').val(data.endereco.bairro),
    $('#complemento').val(data.endereco.complemento),
    $('#cep').val(data.endereco.cep),
    $('#modalAdd').modal('show')
@endsection
@section('linhaAtualizar')
    linhaAtualizar[0].cells[1].textContent = atualizarItem.cliente.nomeCompleto;
    linhaAtualizar[0].cells[2].textContent = atualizarItem.plano.nome;
    linhaAtualizar[0].cells[3].textContent = atualizarItem.dataContratacao;
    linhaAtualizar[0].cells[4].textContent = atualizarItem.diaVencimento;
@endsection

@section('js')
    <script>
        $(function () {
            $(function () {
                $('#cep').inputmask('99999-999');
            })

            $('#cep').focusout(function(){
                $.getJSON("https://viacep.com.br/ws/"+ $('#cep').val() +"/json/?callback=?", function(dados) {
                    if (!("erro" in dados)) {
                        console.log(dados);

                        $("#cidade").val(dados.localidade);
                        $("#estado").val(dados.uf);
                        $("#rua").val(dados.logradouro);
                        $("#bairro").val(dados.bairro);

                    }
                    else {

                        $('#cep').val("")
                        alert("CEP não encontrado.");

                    }
                });
            });

            $('#formularioAdd').validate({
                errorElement: "em",
                validClass: "success",
                errorClass: "invalid text-danger small border-danger",
                rules: {
                    cliente_id: {
                        required: true  
                    },
                    plano_id: {
                        required: true  
                    },
                    data_contratacao: {
                        required: true
                    },
                    data_vencimento: {
                        required: true
                    },
                    cidade: {
                        required: true  
                    },
                    estado: {
                        required: true  
                    },
                    rua: {
                        required: true  
                    },
                    bairro: {
                        required: true  
                    },
                    complemento: {
                        required: true  
                    },
                    cep: {
                        required: true  
                    }
                },
                messages: {
                    cliente_id: {
                        required: "Selecione o cliente"  
                    },
                    plano_id: {
                        required: "Selecione o plano"    
                    },
                    data_contratacao: {
                        required: "Informe o dia da contratação"  
                    },
                    data_vencimento: {
                        required: "Informe o dia da cobrança"  
                    },
                    cidade: {
                        required: "Cidade é obrigatório"  
                    },
                    estado: {
                        required: "Estado é obrigatório"  
                    },
                    rua: {
                        required: "Rua é obrigatório"  
                    },
                    bairro: {
                        required: "Bairro é obrigatório"  
                    },
                    complemento: {
                        required: "Complemento é obrigatório"  
                    },
                    cep: {
                        required: "CEP é obrigatório"  
                    }
                }
            })
        });
    </script>
@endsection