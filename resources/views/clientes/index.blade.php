@extends('layouts.principal')
<!--Titulo-->
@section('titulo', 'Clientes')

<!--Titulo Listagem-->
@section('tituloLista', 'Clientes Cadastrados')

<!--Menus do nivel de acesso-->
@section('menus')
    @foreach ($menus as $menu)
        <a type="button" class="btn btn-primary m-1 text-light" href="{{$menu->link}}">{{$menu->nome}}</a>
    @endforeach
@endsection

<!-- Tipos de pesquisa-->
@section('tiposPesquisa')
    <option value="nomeCompleto">Nome</option>
    <option value="cpfcnpj">Documento</option>
    <option value="email">E-mail</option>
    <option value="telefone">Celular</option>
    <option value="status">Status</option>
@endsection

<!--Inicio Tabela de Listagem-->
@section('idTabela', 'clientes')
@section('cabecalhoLista')
    <th scope="col">Nome</th>
    <th scope="col">Documento</th>
    <th scope="col">Tipo</th>
    <th scope="col">Status</th>
    <th class="col-lg-2" scope="col">Ações</th>
@endsection

@section('corpoLista')
    @section('getpostjson', 'clientes')
    
    '<tr>' +
        '<td hidden>' + clientes[i].id + '</td>' +
        '<td>' + clientes[i].nomeCompleto + '</td>' +
        '<td>' + clientes[i].cpfcnpj + '</td>' +
        '<td>' + clientes[i].tipo_cliente.nome + '</td>' +
        '<td>' + clientes[i].status +'</td>' +
        '<td class="text-center col-4">' + 
            '<button type="button" class="btn btn-primary mr-1" onclick="perfilCliente('+ clientes[i].id +')">Ver</button>' +
            '<button type="button" class="btn btn-primary mr-1" onclick="editar('+ clientes[i].id +')">Editar</button>' +
            '<button type="button" class="btn btn-danger" onclick="modalExcluir('+ clientes[i].id +')">Excluir</button>' +
        '</td>' +
    '</tr>'
@endsection

<!--Botoes Modal Adicionar Clientes-->
@section('btnAdd', 'Adicionar Cliente')
@section('tituloModal', 'Novo Cliente')

<!--Configurações Botao Salvar Dentro do Modal-->
@section('tipoBtn', 'submit')
@section('nomeBtn', 'Salvar')

<!--Formulario Modal Adicionar Clinte-->
@section('formularioAdicionar')
    <div class="h4 m-4 text-center">Dados pessoais</div>
    <input hidden type="text" class="form-control" name="id" id="id">
    <div class="form-group">
        <label for="nome">Nome completo</label>
        <input type="text" class="form-control required" name="nome" id="nome" placeholder="Nome completo do cliente">
    </div>
    <div class="form-group">
        <label for="dataNasc">Data de Nascimento</label>
        <input type="date" class="form-control" name="dataNasc" id="dataNasc">
    </div>
    <div class="form-group">
        <label for="cpfcnpj">CPF/CNPJ</label>
        <input type="text" class="form-control" name="cpfcnpj" id="cpfcnpj" placeholder="CPF ou CNPJ do cliente">
    </div>
    <div class="form-group">
        <label for="email">E-mail</label>
        <input type="email" class="form-control" name="email" id="email" placeholder="Um ou mais emails do cliente">
    </div>
    <div class="form-group">
        <label for="celular">Celular</label>
        <input type="text" class="form-control" name="celular" id="celular" placeholder="Um ou mais celulares do cliente">
    </div>
    <div class="form-group">
        <label for="tipoCliente">Tipo do Cliente</label>
        <select class="form-control" name="tipoCliente" id="tipoCliente">
            <script>
                $(function() {
                   $.getJSON('/api/tiposClientes', function(data){
                        for (i = 0; i < data.length; i++) {
                            $('#tipoCliente').append('<option value="' + data[i].id + '">' + data[i].nome + '</option>');
                        }
                   }) 
                });
            </script>
        </select>
    </div>

    <div class="h4 m-4 text-center">Endereço</div>

    <div class="form-group">
        <label for="cep">CEP</label>
        <input type="text" class="form-control" name="cep" id="cep" placeholder="CEP do endereço do cliente">
    </div>
    <div class="row">
        <div class="form-group col-9">
            <label for="cidade">Cidade</label>
            <input disabled type="text" class="form-control" name="cidade" id="cidade" placeholder="Cidade do cliente">
        </div>
        <div class="form-group col-3">
            <label for="estado">Estado</label>
            <input disabled type="text" class="form-control" name="estado" id="estado" placeholder="Estado">
        </div>
    </div>
    <div class="form-group">
        <label for="rua">Rua</label>
        <input type="text" class="form-control" name="rua" id="rua" placeholder="Rua do cliente">
    </div>
    <div class="form-group">
        <label for="bairro">Bairro</label>
        <input type="text" class="form-control" name="bairro" id="bairro" placeholder="Bairro do cliente">
    </div>
    <div class="form-group">
        <label for="complemento">Complemento</label>
        <input type="text" class="form-control" name="complemento" id="complemento" placeholder="Complemento do cliente">
    </div>
    <div class="form-group">
        <label for="status">Status</label>
        <select class="form-control" name="status" id="status">
            <option value="Ativo">Ativo</option>
            <option value="Inativo">Inativo</option>
        </select>
    </div>
@endsection

<!--Inicio Modal Dados Cliente-->
<div class="modal fade" id="modalPerfilCliente" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        
        <div class="modal-content">

            <div class="modal-header p-2">
                <h5 class="modal-title font-weight-bold ml-2" id="nomeCliente"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="dadosPessoais" data-toggle="tab" href="#dadoPessoal" role="tab" aria-controls="home" aria-selected="true">Dados pessoais</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="dadosConexao" data-toggle="tab" href="#dadoConexao" role="tab" aria-controls="profile" aria-selected="false">Conexão</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="dadosEndereco" data-toggle="tab" href="#dadoEndereco" role="tab" aria-controls="profile" aria-selected="false">Endereço</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="dadosContrato" data-toggle="tab" href="#dadoContato" role="tab" aria-controls="contact" aria-selected="false">Contrato</a>
                </li>
            </ul>
            <div class="tab-content m-3" id="myTabContent">
                <div class="tab-pane fade show active" id="dadoPessoal" role="tabpanel" aria-labelledby="dadosPessoais">
                    <div class="row">
                        <div class="form-group col-7">
                            <label>Nome completo</label>
                            <input disabled type="text" class="form-control" id="perfilNome">
                        </div>
                        <div class="form-group col-5">
                            <label>Tipo</label>
                            <input disabled type="text" class="form-control" id="perfilTipoCliente">
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="form-group col-7">
                            <label>CPF/CNPJ</label>
                            <input disabled type="text" class="form-control" id="perfilCpfcnpj">
                        </div>
                        <div class="form-group col-5">
                            <label>Data de Nasc.</label>
                            <input disabled type="date" class="form-control" id="perfilDataNasc">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-7">
                            <label for="email">E-mail</label>
                            <input disabled type="email" class="form-control" name="email" id="perfilEmail" placeholder="Um ou mais emails do cliente">
                        </div>
                        <div class="form-group col-5">
                            <label for="celular">Celular</label>
                            <input disabled type="text" class="form-control" name="celular" id="perfilCelular" placeholder="Um ou mais celulares do cliente">
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="dadoConexao" role="tabpanel" aria-labelledby="conexao">
                    @php
                        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);

                        echo $hostname;
                        echo gethostname();
                    @endphp     
                </div>
                <div class="tab-pane fade" id="dadoEndereco" role="tabpanel" aria-labelledby="dadosEndereco">
                    <div class="row">
                        <div class="form-group col-4">
                            <label>CEP</label>
                            <input disabled type="text" class="form-control" id="perfilCep">
                        </div>
                        <div class="form-group col-5">
                            <label>Cidade</label>
                            <input disabled type="text" class="form-control" id="perfilCidade">
                        </div>
                        <div class="form-group col-3">
                            <label>Estado</label>
                            <input disabled type="text" class="form-control" id="perfilEstado">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-6">
                            <label>Rua</label>
                            <input disabled type="text" class="form-control" id="perfilRua">
                        </div>
                        <div class="form-group col-6">
                            <label>Bairro</label>
                            <input disabled type="text" class="form-control" id="perfilBairro">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label>Complemento</label>
                        <input disabled type="text" class="form-control" id="perfilComplemento">
                    </div>
                </div>
                <div class="tab-pane fade" id="dadoContato" role="tabpanel" aria-labelledby="dadosContrato">
                    <ul class="row list-group" id="listaPlanos">
                </div>
            </div>

        </div>
    </div>
</div>
<!--Fim Modal Dados Cliente-->

<!--Modal Dados do Cliente-->
<div class="modal fade" id="modalPlanoCliente" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title font-weight-bold ml-2">Dados do Contrato</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

                <div class="h4 text-center">Plano contratado</div>
                <div class="form-group">
                    <label>Plano</label>
                    <input disabled type="text" class="form-control" id="nomePlano">
                </div>
                <div class="form-group">
                    <label>Descrição</label>
                    <input disabled type="text" class="form-control" id="descricaoPlano">
                </div>
                <div class="row">
                    <div class="form-group col-6">
                        <label>Velocidade</label>
                        <input disabled type="text" class="form-control" id="velocidadePlano">
                    </div>
                
                    <div class="form-group col-6">
                        <label>Status</label>
                        <input disabled type="text" class="form-control" id="statusPlano">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-6">
                        <label>Data de contratação</label>
                        <input disabled type="date" class="form-control" id="data_contratacao">
                    </div>
                
                    <div class="form-group col-6">
                        <label>Dia do Vencimento</label>
                        <input disabled type="text" class="form-control" id="data_vencimento">
                    </div>
                </div>

                <div class="h4 m-4 text-center">Endereço do plano</div>
                <div class="form-group">
                    <label>CEP</label>
                    <input disabled type="text" class="form-control" id="contratoCep">
                </div>
                <div class="row">
                    <div class="form-group col-9">
                        <label>Cidade</label>
                        <input disabled type="text" class="form-control" id="contratoCidade">
                    </div>
                    <div class="form-group col-3">
                        <label>Estado</label>
                        <input disabled type="text" class="form-control" id="contratoEstado">
                    </div>
                </div>
                <div class="form-group">
                    <label>Rua</label>
                    <input disabled type="text" class="form-control" id="contratoRua">
                </div>
                <div class="form-group">
                    <label>Bairro</label>
                    <input disabled type="text" class="form-control" id="contratoBairro">
                </div>
                <div class="form-group">
                    <label>Complemento</label>
                    <input disabled type="text" class="form-control" id="contratoComplemento">
                </div>

            </div>


        </div>
    </div>
</div>

<!--Javascript Adicionar Cliente-->
@section('nomeNovoItem', 'novoCliente')
@section('conteudoNovoItem')
    nomeCompleto: $('#nome').val(),
    dataNascimento: $('#dataNasc').val(),   
    cpfcnpj: $('#cpfcnpj').val(),
    email: $('#email').val(),
    telefone: $('#celular').val(),
    tipoCliente_id: $('#tipoCliente').val(),
    cidade: $('#cidade').val(),
    estado: $('#estado').val(),
    rua: $('#rua').val(),
    bairro: $('#bairro').val(),
    complemento: $('#complemento').val(),
    cep: $('#cep').val(),
    status: $('#status').val()
@endsection

<!--Javascript Atualizar Clientes-->
@section('atualizarTabela')
    '<tr>' +
        '<td hidden>' + novoItem.id + '</td>' +
        '<td>' + novoItem.nomeCompleto + '</td>' +
        '<td>' + novoItem.cpfcnpj + '</td>' +
        '<td>' + novoItem.tipo_cliente.nome + '</td>' +
        '<td>' + novoItem.status +'</td>' +
        '<td class="text-center">' + 
            '<button type="button" class="btn btn-primary mr-1" onclick="perfilCliente('+ novoItem.id +')">Ver</button>' +
            '<button type="button" class="btn btn-primary mr-1" onclick="editar('+ novoItem.id +')">Editar</button>' +
            '<button type="button" class="btn btn-danger" onclick="modalExcluir('+ novoItem.id +')">Excluir</button>' +
        '</td>' +
    '</tr>'
@endsection

<!--Javascript Zerar inputs-->
@section('zerarInputs')
    $('#id').val(''),
    $('#nome').val(''),
    $('#cpfcnpj').val(''),
    $('#dataNasc').val(''),
    $('#email').val(''),
    $('#celular').val(''),
    $('#tipoCliente').val(''),
    $('#cidade').val(''),
    $('#estado').val(''),
    $('#rua').val(''),
    $('#bairro').val(''),
    $('#complemento').val(''),
    $('#cep').val(''),
    $('#status').val('')
@endsection

<!--Javascript Editar Etapa-->
@section('carregarDadosEditar')
    $('#id').val(data.id),
    $('#nome').val(data.nomeCompleto),
    $('#dataNasc').val(data.dataNascimento),
    $('#cpfcnpj').val(data.cpfcnpj),
    $('#email').val(data.email),
    $('#celular').val(data.telefone),
    $('#tipoCliente').val(data.tipoCliente_id),
    $('#cidade').val(data.cidade),
    $('#estado').val(data.estado),
    $('#rua').val(data.rua),
    $('#bairro').val(data.bairro),
    $('#complemento').val(data.complemento),
    $('#cep').val(data.cep),
    $('#status').val(data.status),
    $('#modalAdd').modal('show')
@endsection

@section('linhaAtualizar')
    linhaAtualizar[0].cells[1].textContent = atualizarItem.nomeCompleto;
    linhaAtualizar[0].cells[2].textContent = atualizarItem.cpfcnpj;
    linhaAtualizar[0].cells[3].textContent = atualizarItem.tipo_cliente.nome;
    linhaAtualizar[0].cells[4].textContent = atualizarItem.status;
@endsection

@section('js')

    {{-- Script de inputmask e pesquisar por cep --}}
    <script>
        $(function () {

            $('#cpfcnpj').inputmask({
                mask: ["999.999.999-99", "99.999.999/9999-99",],
                placeholder: "",
            });

            $('#celular').inputmask({
                mask: ["9999-9999", "(99) 9 9999-9999", ],
                placeholder: "",
            });

            $('#cep').inputmask({
                mask: '99999-999',
                placeholder: "",
            });

        })

        $('#cep').focusout(function(){
            $.getJSON("https://viacep.com.br/ws/"+ $('#cep').val() +"/json/?callback=?", function(dados) {
                if (!("erro" in dados)) {
                    $("#cidade").val(dados.localidade);
                    $("#estado").val(dados.uf);
                    $("#rua").val(dados.logradouro);
                    $("#bairro").val(dados.bairro);
                }
                else {
                    $('#cep').val("")
                    alert("CEP não encontrado.");
                }
            });
        });
    </script>

    {{-- Script de perfil de cliente --}}
    <script>
        function modalPlano(id) {
            $("#modalPlanoCliente").modal('show');

            $.getJSON('/api/contratos/'+id, function(data){
                console.log(data);
                //Dados do plano
                $('#nomePlano').val(data.plano.nome);
                $('#descricaoPlano').val(data.plano.descricao);
                $('#velocidadePlano').val(data.plano.velocidade);
                $('#statusPlano').val(data.plano.status);
                $('#data_contratacao').val(data.dataContratacao);
                $('#data_vencimento').val(data.diaVencimento);

                //Dados de endereço
                $('#contratoCep').val(data.endereco.cep);
                $('#contratoCidade').val(data.endereco.cidade);
                $('#contratoEstado').val(data.endereco.estado);
                $('#contratoRua').val(data.endereco.rua);
                $('#contratoBairro').val(data.endereco.bairro);
                $('#contratoComplemento').val(data.endereco.complemento);

            });
        }

        function perfilCliente(id) {
            $("#modalPerfilCliente").modal('show');

            $.getJSON('/api/clientes/'+id, function(data){
                console.log(data);  
                // Dados pessoais
                $('#nomeCliente').text("Perfil " + data.nomeCompleto);
                $('#perfilNome').val(data.nomeCompleto);
                $('#perfilTipoCliente').val(data.tipo_cliente.nome);
                $('#perfilDataNasc').val(data.dataNascimento);
                $('#perfilCpfcnpj').val(data.cpfcnpj);
                $('#perfilEmail').val(data.email);
                $('#perfilCelular').val(data.telefone);

                //Endereço
                $('#perfilCep').val(data.cep);
                $('#perfilCidade').val(data.cidade);
                $('#perfilEstado').val(data.estado);
                $('#perfilRua').val(data.rua);
                $('#perfilBairro').val(data.bairro);
                $('#perfilComplemento').val(data.complemento);

                //Contrato
                $('#listaPlanos').html("");
                data['contratos'].forEach(contrato => {
                    $('#listaPlanos').append("\
                        <li class='list-group-item'>\
                            " + contrato.endereco.rua + " - \
                            " + contrato.endereco.cidade + "\
                            <button type='button' class='btn btn-primary' onclick='modalPlano("+ contrato.id +")'>Ver</button>\
                        </li>"
                    );
                });

            });
        }
    </script>

    {{-- Script de validação --}}
    <script>
        $(function () {
            $('#formularioAdd').validate({
                errorElement: "em",
                validClass: "success",
                errorClass: "invalid text-danger small border-danger",
                rules: {
                    nome: {
                        required: true  
                    },
                    dataNasc: {
                        required: true  
                    },
                    cpfcnpj: {
                        required: true,
                        minlength: 14
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    celular: {
                        required: true  
                    },
                    tipoCliente: {
                        required: true
                    },
                    cidade: {
                        required: true  
                    },
                    estado: {
                        required: true  
                    },
                    rua: {
                        required: true  
                    },
                    bairro: {
                        required: true  
                    },
                    complemento: {
                        required: true  
                    },
                    cep: {
                        required: true,
                        minlength: 9
                    },
                    status: {
                        required: true  
                    }
                },
                messages: {
                    nome: {
                        required: "Nome é obrigatório"
                    },
                    dataNasc: {
                        required: "Data de nascimento é obrigatório"  
                    },
                    cpfcnpj: {
                        required: "CPF ou CNPJ é obrigatório",
                        minlength: jQuery.validator.format("Digite um documento valido")
                    },
                    email: {
                        required: "E-mail é obrigatório",
                        email: "E-mail invalido"
                    },
                    celular: {
                        required: "Celular é obrigatório",
                    },
                    tipoCliente: {
                        required: "Tipo do cliente é obrigatório",
                    },
                    cidade: {
                        required: "Cidade é obrigatório"  
                    },
                    estado: {
                        required: "Estado é obrigatório"  
                    },
                    rua: {
                        required: "Rua é obrigatório"  
                    },
                    bairro: {
                        required: "Bairro é obrigatório",
                    },
                    complemento: {
                        required: "Complemento é obrigatório"  
                    },
                    cep: {
                        required: "Cep é obrigatório",
                        minlength: jQuery.validator.format("Digite CEP valido")
                    },
                    status: {
                        required: "Status é obrigatório"  
                    }
                }
            })
        });
    </script>
@endsection