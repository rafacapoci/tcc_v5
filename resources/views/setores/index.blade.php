@extends('layouts.principal')
<!--Titulo--> 
@section('titulo', 'Setores')
<!--Titulo Listagem-->
@section('tituloLista', 'Setores Cadastrados')
<!--Menus do nivel de acesso-->
@section('menus')
    @foreach ($menus as $menu)
        <a type="button" class="btn btn-primary m-1 text-light" href="{{$menu->link}}">{{$menu->nome}}</a>
    @endforeach
@endsection
<!-- Tipos de pesquisa-->
@section('tiposPesquisa')
    <option value="nome">Nome</option>
    <option value="descricao">Descriçao</option>
@endsection
<!--Inicio Tabela de Listagem-->
@section('idTabela', 'setores')
@section('cabecalhoLista')
    <th scope="col">Nome</th>
    <th scope="col">Descrição</th>
    <th class="col-lg-2" scope="col">Ações</th>
@endsection
@section('corpoLista')
    @section('getpostjson', 'setores')
    '<tr>' +
        '<td hidden>' + setores[i].id + '</td>' +
        '<td>' + setores[i].nome + '</td>' +
        '<td>' + setores[i].descricao + '</td>' +
        '<td class="text-center">' + 
            '<button type="button" class="btn btn-primary mr-1" onclick="editar('+ setores[i].id +')">Editar</button>' +
            '<button type="button" class="btn btn-danger" onclick="modalExcluir('+ setores[i].id +')">Excluir</button>' +
        '</td>' +
    '</tr>'
@endsection
<!--Botoes Modal Adicionar Plano-->
@section('btnAdd', 'Adicionar Setor')
@section('tituloModal', 'Novo Setor')
<!--Configurações Botao Salvar Dentro do Modal-->
@section('tipoBtn', 'submit')
@section('nomeBtn', 'Salvar')
<!--Formulario Modal Adicionar Plano-->
@section('formularioAdicionar')
    <input hidden type="text" class="form-control" name="id" id="id">
    <div class="form-group">
        <label for="nome">Nome</label>
        <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome do novo setor">
    </div>
    <div class="form-group">
        <label for="descricao">Descrição</label>
        <textarea name="descricao" id="descricao" class="form-control" cols="10" rows="6" placeholder="Descrição sobre novo setor"></textarea>
    </div>
@endsection
<!--Javascript Adicionar Setor-->
@section('nomeNovoItem', 'novoSetor')
@section('conteudoNovoItem')
    nome: $('#nome').val(),
    descricao: $('#descricao').val()
@endsection
<!--Javascript Atualizar Etapa-->
@section('atualizarTabela')

    '<tr>' +
        '<td hidden>' + novoItem.id + '</td>' +
        '<td>' + novoItem.nome + '</td>' +
        '<td>' + novoItem.descricao + '</td>' +
        '<td class="text-center">' + 
            '<button type="button" class="btn btn-primary mr-1" onclick="editar('+ novoItem.id +')">Editar</button>' +
            '<button type="button" class="btn btn-danger" onclick="modalExcluir('+ novoItem.id +')">Excluir</button>' +
        '</td>' +
    '</tr>'

@endsection
<!--Javascript Zerar inputs-->
@section('zerarInputs')
    $('#id').val(''),
    $('#nome').val(''),
    $('#descricao').val('')
@endsection
<!--Javascript Editar Etapa-->
@section('carregarDadosEditar')
    $('#id').val(data.id),
    $('#nome').val(data.nome),
    $('#descricao').val(data.descricao),
    $('#modalAdd').modal('show')
@endsection
@section('linhaAtualizar')
    linhaAtualizar[0].cells[1].textContent = atualizarItem.nome;
    linhaAtualizar[0].cells[2].textContent = atualizarItem.descricao;
@endsection

@section('js')
    <script>
        $(function () {
            $('#formularioAdd').validate({
                errorElement: "em",
                validClass: "success",
                errorClass: "invalid text-danger small border-danger",
                rules: {
                    nome: {
                        required: true  
                    },
                    descricao: {
                        required: true  
                    },
                },
                messages: {
                    nome: {
                        required: "Nome é obrigatório"
                    },
                    descricao: {
                        required: "Descrição é obrigatório"  
                    },
                }
            })
        });
    </script>
@endsection