<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>@yield('titulo')</title>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.7/jquery.inputmask.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    

</head>
<body>   

<!--Inicio Container -->
<div class="container-fluid">

<!-- -------------------------------------------------------------------------------------------------------------- -->

    <!--Inicio Linha UM -->
    <div class="row mt-2">
        <!--Inicio coluna UM -->
        <div class="col-12 ">

            <!--Inicio barra de menu (navbar) -->
            <nav class="navbar-light navbar-expand-md">

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!--Inicio dos botões -->
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        @yield('menus')
                    </ul>
                    <div>
                        <ul class="navbar-nav ">
                            <li class="btn p-0 btn-sm btn-primary nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-white" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                    Opções
                                </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        Sair
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div>        
                </div>
                <!--Fim dos botões -->

            </nav>
            <!--Fim barra de menu (navbar) -->

        </div>
        <!--Fim coluna 1 (um) -->
    </div>
    <!--Fim Linha 1 (um) -->

<!-- -------------------------------------------------------------------------------------------------------------- -->

    <!--Inicio Linha 2 (dois/LISTAGEM) -->
    <div class="row justify-content-center m-4">
        <!--Inicio coluna 2 (dois) -->
        <div class="col-12 mb-5">
            <p class="h2 mt-5 mb-4 text-center">@yield('tituloLista')</p>

            <!--Inicio Barra Pesquisa -->
            <div class="mb-4 text-left">
                <form method="get" class="d-flex input-group btn-group">
                    <select class="col-2 custom-select" id="tipoPesquisa" onchange="pesquisar()">
                        @yield('tiposPesquisa')
                    </select>
                    <input class="form-control" type="text" id="pesquisa" name="pesquisa" 
                        placeholder="Digite para buscar..." onkeyup="pesquisar()">
                </form>
            </div>
            <!--FIM Barra Pesquisa -->

            <!--Inicio botão para abrir Modal-->
            <div class="mb-2 text-right">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalAdd" onclick="@yield('zerarInputs')">
                    @yield('btnAdd')
                </button>
            </div>
            <!--Fim botão para abrir Modal-->

            <!--Inicio Lista -->
            <table class="table table-hover table-bordered mb-5" id="@yield('idTabela')">
                <thead class="text-center">
                    @yield('cabecalhoLista')
                </thead>
                <tbody>
                    
                </tbody> 
            </table>
            <!--Fim Lista -->
        </div>
        <!--Fim coluna 2 (dois) -->
    </div>
    <!--Fim Linha 2 (dois/LISTAGEM) -->

<!-- -------------------------------------------------------------------------------------------------------------- -->

    <!--Inicio Form Modal Adicionar-->
    <form  method="post" id="formularioAdd">
        @csrf
        <div class="modal fade" id="modalAdd" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                
                <div class="modal-content">

                    <div class="modal-header justify-content-center">
                        <h5 class="modal-title font-weight-bold" id="exampleModalLabel">@yield('tituloModal')</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <div class="modal-body">
                                
                            @yield('formularioAdicionar')
                                
                        </div>

                        <div class="modal-footer">
                            <button type="cancel" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                            <button type="@yield('tipoBtn')" @yield('configuracoesBotaoSalvar') id="salvar" class="btn btn-success">@yield('nomeBtn')</button>
                        </div>
                </div>
            </div>
        </div>

        @yield('modal2')
        @yield('modal3')
        @yield('modal4')
    </form>
    <!--Fim Form Modal Adicionar-->

    <!--Inicio Modal Deletar-->
    <div class="modal fade" id="modalDeletar" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
            
            <div class="modal-content">
                <div class="modal-body text-center">
                    <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Atenção!</h5>
                    <p>
                        Tem certeza que deseja excluir?<br>
                        Essa ação não poderá ser desfeita!
                    </p>
                    <button type="button" id="btnModalExcluir" class="btn btn-danger">Excluir</button>
                    <button type="cancel" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
    <!--Fim Modal Deletar-->

</div>
<!--Fim Container -->

</body>
</html>

@yield('js')

<script>

    $(function(){  

        //Função para gerar token, necessario para metodos post----------------------------------------
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });

        //Função para cadastrar novo item (STORE)------------------------------------------------------
        $("#formularioAdd").submit(
        function(event){
            event.preventDefault();

            if ($("#id").val() != '') { 
                @yield('nomeNovoItem') = { //essa variavel está 
                    id: $('#id').val(),
                    @yield('conteudoNovoItem')
                };

                $.ajax({
                    type: "PUT",
                    url: "/api/@yield('getpostjson')/" + @yield('nomeNovoItem').id,
                    context: this,
                    data: @yield('nomeNovoItem'), //sendo recebida aqui
                    success: function(data){
                        atualizarItem = JSON.parse(data);

                        linhas = $('#@yield('idTabela') > tbody > tr');

                        linhaAtualizar = linhas.filter(function(i, elemento) {
                                return elemento.cells[0].textContent == atualizarItem.id;
                        });
                        if (linhaAtualizar) {
                            @yield('linhaAtualizar')
                        }
                        $("#modalAdd").modal('hide');
                        @yield('zerarInputs');
                        toastr.success("Atualizado com sucesso.");
                    },
                    error: function(error){
                        console.log(error);
                        toastr.error(error.responseJSON.message);
                    }
                });

            } else {
                @yield('nomeNovoItem') = {
                    @yield('conteudoNovoItem')
                };
                $.ajax({
                    type: "POST",
                    url: "/api/@yield('getpostjson')/",
                    context: this,
                    data: @yield('nomeNovoItem'), //sendo recebida aqui
                    success: function(data){
                        novoItem = JSON.parse(data);
                        var linha =  @yield('atualizarTabela')
                            '<td class="text-center">' + 
                                '<button type="button" class="btn btn-primary mr-1" onclick="editar('+ novoItem.id +')">Editar</button>' +
                                '<button type="button" class="btn btn-danger" onclick="modalExcluir('+ novoItem.id +')">Excluir</button>' +
                            '</td>' +
                        '</tr>';

                        $('#@yield('idTabela') > tbody').prepend(linha);

                        $("#modalAdd").modal('hide');
                        @yield('zerarInputs');
                        toastr.success("Cadastrado com sucesso.");
                    },
                    error: function(error){
                        toastr.error(error.responseJSON.message);
                    }
                });
            }

            
        });

        //Função para carregar lista de itens cadastrados (INDEX)--------------------------------------
        $.getJSON('/api/@yield('getpostjson')', function(@yield('getpostjson')){
            for (i = 0; i < @yield('getpostjson').length; i++){
                var linha = @yield('corpoLista');
                $('#@yield('idTabela') > tbody').prepend(linha);
            }
        });

    });

    //Função para pesquisar na lista de itens cadastrados (INDEX)--------------------------------------
    function pesquisar(){

        $.getJSON('/@yield('getpostjson')/' + $('#tipoPesquisa').val() + '/' + $('#pesquisa').val(), function(@yield('getpostjson')) {
            $('#@yield('idTabela') > tbody').html('');

            for (let i = 0; i < @yield('getpostjson').length; i++) {
                var linha = @yield('corpoLista');
                $('#@yield('idTabela') > tbody').append(linha);
            }

            if ($('#pesquisa').val() == "") {
                $('#@yield('idTabela') > tbody').html('');
                for (i = 0; i < @yield('getpostjson').length; i++){
                    var linha = @yield('corpoLista');
                        
                    $('#@yield('idTabela') > tbody').append(linha);
                }
            }

        });
    }

    //Função para pegar dados a editar do item-----------------------------------------------------------------------------
    function editar(id){
        $.getJSON('/api/@yield('getpostjson')/'+id, function(data){
            @yield('carregarDadosEditar')
        });
    }

    //Função para excluir item -----------------------------------------------------------------------
    function modalExcluir(id) {
        $("#modalDeletar").modal("show");
        $("#btnModalExcluir").attr('onclick', 'excluir('+ id +')');
    }

    //Função para excluir item -----------------------------------------------------------------------
    function excluir(id){
        $.ajax({
            type: "DELETE",
            url: "/api/@yield('getpostjson')/" + id,
            context: this,
            success: function(){
                linhas = $('#@yield('idTabela')>tbody>tr');
                linhaDeletar = linhas.filter(
                    function(i, elemento) {
                        return elemento.cells[0].textContent == id;
                    });
                if (linhaDeletar) {
                    linhaDeletar.remove();
                    $("#modalDeletar").modal("hide");
                }
                toastr.success("Excluido.");
            },
            error: function(error){
                console.log(error);
                toastr.error("Ops... talvez este item esteja sendo referenciado em outro lugar e não pode ser deletado agora.");
            }
        });
    }
    
</script>