@extends('layouts.principal')

<!--Titulo-->
@section('titulo', 'Inicio - Ordens de serviço')

<!--Titulo Listagem-->
@section('tituloLista', 'Ordens de serviço em aberto')

<!--Menus do nivel de acesso-->
@section('menus')
    @foreach ($menus as $menu)
        <a type="button" class="btn btn-primary m-1 text-light" href="{{$menu->link}}">{{$menu->nome}}</a>
    @endforeach
@endsection

<!-- Tipos de pesquisa-->
@section('tiposPesquisa')
    <option value="true">Abertos</option>
    <option value="false">Fechados</option>
@endsection

<!--Inicio Tabela de Listagem-->
@section('idTabela', 'atendimentos')
@section('cabecalhoLista')
    <th scope="col">Cliente</th>
    <th scope="col">Documento</th>
    <th scope="col">Abertura</th>
    <th scope="col">Problema</th>
    <th class="col-lg-2" scope="col">Ações</th>
@endsection

@section('corpoLista')
    @section('getpostjson', 'atendimentos')
    '<tr>' +
        '<td hidden>' + atendimentos[i].id + '</td>' +
        '<td>' + atendimentos[i].contrato.cliente.nomeCompleto + '</td>' +
        '<td>' + atendimentos[i].contrato.cliente.cpfcnpj + '</td>' +
        '<td>' + atendimentos[i].dataAbertura + '</td>' +
        '<td>' + atendimentos[i].tipo_ocorrencia.nome + '</td>' +
        '<td class="text-center">' + 
            '<button type="button" class="btn btn-primary mr-1" onclick="editar('+ atendimentos[i].id +')">Ver</button>' +
        '</td>' +
    '</tr>'
@endsection

<!--Botoes Modal Adicionar Atendimento-->
@section('btnAdd', 'Iniciar atendimento')
@section('tituloModal', 'Novo Atendimento')

<!--Configurações Botao Salvar Dentro do Modal-->
@section('tipoBtn', 'button')
@section('nomeBtn', 'Proximo')
@section('configuracoesBotaoSalvar', "data-toggle=modal data-target=#modalAddAtendimentoPt2 data-dismiss=modal")

<!--Formulario Modal Adicionar Atendimento-->
@section('formularioAdicionar')
    <input hidden type="text" class="form-control" name="id" id="id">

    <div class="form-group">
        <label for="cliente">Pesquisar Cliente</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <select class="input-group-text" id="tipoPesquisaCliente">
                    <option value="cpfcnpj">Documento</option>
                    <option value="nomeCompleto">Nome</option>
                </select>
            </div>
            <input class="form-control" id="cliente" onkeyup="pesquisarCliente()">
        </div>
    </div>
    
    <div class="form-group">
        <label>Usuarios encontrados</label>
        <select class="form-control" id="clientesLista" onclick="pesquisarContrato()">
        </select>
    </div>

    <div class="form-group">
        <label>Contratos para este usuario</label>
        <select class="form-control" id="contratosLista">
        </select>
    </div>

@endsection

@section('modal2')
    <div class="modal fade" id="modalAddAtendimentoPt2" data-backdrop="static" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            
            <div class="modal-content">

                <div class="modal-header justify-content-center">
                    <h5 class="modal-title font-weight-bold" id="exampleModalLabel">@yield('tituloModal')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>


                <div class="modal-body">
                    <div class="form-group">
                        <label for="setor_id">Setor</label>
                        <select class="form-control" name="setor_id" id="setor_id">
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tipoOcorrencia_id">Tipo da ocorrencia</label>
                        <select class="form-control" name="tipoOcorrencia_id" id="tipoOcorrencia_id">
                        </select>
                    </div>                    
                    <div class="form-group">
                        <label for="etapa_id">Etapa</label>
                        <select class="form-control" name="etapa_id" id="etapa_id">
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="velocidade">Descrição da ocorrencia</label>
                        <textarea name="descricao" id="descricao" class="form-control" cols="10" rows="6" placeholder="Relato do cliente"></textarea>
                    </div>
                        
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalAdd" data-dismiss="modal">Voltar</button>
                    <button type="submit" id="salvar" class="btn btn-success">Salvar</button>
                </div>

            </div>
        </div>
    </div>
@endsection

<!--Javascript Adicionar Cliente-->
@section('nomeNovoItem', 'novoAtendimento')
@section('conteudoNovoItem')
    etapa_id: $('#etapa_id').val(),   
    contrato_id: $('#contratosLista').val(),   
    tipoOcorrencia_id: $('#tipoOcorrencia_id').val(),
    descricaoOcorrencia: $('#descricao').val()
@endsection

<!--Javascript Atualizar Clientes-->
@section('atualizarTabela')
    '<tr>' +
        '<td hidden>' + novoItem.id + '</td>' +
        '<td>' + novoItem.contrato.cliente.nomeCompleto + '</td>' +
        '<td>' + novoItem.contrato.cliente.cpfcnpj + '</td>' +
        '<td>' + novoItem.dataAbertura + '</td>' +
        '<td>' + novoItem.tipo_ocorrencia.nome + '</td>' +
        '<td class="text-center">' + 
            '<button type="button" class="btn btn-primary mr-1" onclick="editar('+ novoItem.id +')">Ver</button>' +
        '</td>' +
    '</tr>'
@endsection

@section('zerarInputs')
    $('#clientesLista').val(''),
    $('#contratosLista').val(''),   
    $('#setor_id').val(''),
    $('#tipoOcorrencia_id').val(''),
    $('#etapa_id').val(''),
    $('#descricao').val('')
@endsection

@section('js')
    <script>
        $(function() {
            $('#cliente').inputmask({
                mask: ["999.999.999-99", "99.999.999/9999-99", "a{40}",],
                placeholder: "",
            });

            $.getJSON('/api/tiposOcorrencias', function(data){
                for (i = 0; i < data.length; i++) {
                    $('#tipoOcorrencia_id').append('<option value="' + data[i].id + '">' + data[i].nome + '</option>');
                }
            })

            $.getJSON('/api/setores', function(data){
                for (i = 0; i < data.length; i++) {
                    $('#setor_id').append('<option value="' + data[i].id + '">' + data[i].nome + '</option>');
                }
            })

            $.getJSON('/api/etapas', function(data){
                for (i = 0; i < data.length; i++) {
                    $('#etapa_id').append('<option value="' + data[i].id + '">' + data[i].nome + '</option>');
                }
            })
        });

        function pesquisarCliente() {
            $.getJSON('/clientes/' + $('#tipoPesquisaCliente').val() + '/' + $('#cliente').val(), function(clientes){
                $('#clientesLista').html('');
                $('#clientesLista').append('<option>Clique para ver os clientes encontrados</option>');
                clientes.forEach(cliente => {
                    $('#clientesLista').append('<option value=' + cliente.id + '>' + cliente.nomeCompleto + '</option>');
                });
                if ($('#cliente').val() == null || $('#cliente').val() == '') {
                    $('#clientesLista').html('');
                }
            }) 
        }

        function pesquisarContrato() {
            $("#contratosLista").html("");
            $.getJSON('/contratos/' + 'cliente_id' + '/' + $('#clientesLista').val(), function(contratos){
                contratos.forEach(contrato => {
                    $('#contratosLista').append('<option value=' + contrato.id + '>' + 'Plano: ' + contrato.plano.nome + '. Endereço: ' + contrato.endereco.bairro + '</option>');
                });
            }) 
        }
    </script>
@endsection