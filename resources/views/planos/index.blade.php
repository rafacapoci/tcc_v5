@extends('layouts.principal')
<!--Titulo-->
@section('titulo', 'Planos')
<!--Titulo Listagem-->
@section('tituloLista', 'Planos Cadastrados')
<!--Menus do nivel de acesso-->
@section('menus')
    @foreach ($menus as $menu)
        <a type="button" class="btn btn-primary m-1 text-light" href="{{$menu->link}}">{{$menu->nome}}</a>
    @endforeach
@endsection
<!-- Tipos de pesquisa-->
@section('tiposPesquisa')
    <option value="nome">Nome</option>
    <option value="descricao">Descriçao</option>
    <option value="velocidade">Velocidade</option>
    <option value="status">Status</option>
@endsection
<!--Tipos Disponiveis de Pesquisar-->
@section('tiposPesquisa')
    <option value="nome">Nome</option>
    <option value="descricao">Descrição</option>
    <option value="velocidade">Velocidade</option>
    <option value="1">Planos ativos</option>
    <option value="0">Planos inativos</option>
@endsection
<!--Inicio Tabela de Listagem-->
@section('idTabela', 'planos')
@section('cabecalhoLista')
    <th scope="col">Nome</th>
    <th scope="col">Velocidade</th>
    <th scope="col">Descrição</th>
    <th scope="col">Preço</th>
    <th scope="col">Status</th>
    <th class="col-lg-2" scope="col">Ações</th>
@endsection
@section('corpoLista')
    @section('getpostjson', 'planos')
        '<tr>' +
            '<td hidden>' + planos[i].id + '</td>' +
            '<td>' + planos[i].nome + '</td>' +
            '<td>' + planos[i].velocidade + '</td>' +
            '<td>' + planos[i].descricao + '</td>' +
            '<td>' + planos[i].preco + '</td>' +
            '<td>' + planos[i].status + '</td>' +
            '<td class="text-center">' + 
                '<button type="button" class="btn btn-primary mr-1" onclick="editar('+ planos[i].id +')">Editar</button>' +
                '<button type="button" class="btn btn-danger" onclick="modalExcluir('+ planos[i].id +')">Excluir</button>' +
            '</td>' +
        '</tr>'    
@endsection
<!--Botoes Modal Adicionar Plano-->
@section('btnAdd', 'Adicionar Plano')
@section('tituloModal', 'Novo Plano')
<!--Configurações Botao Salvar Dentro do Modal-->
@section('tipoBtn', 'submit')
@section('nomeBtn', 'Salvar')
<!--Formulario Modal Adicionar Plano-->
@section('formularioAdicionar')
    <input hidden type="text" class="form-control" name="id" id="id">
    <div class="form-group">
        <label for="nome">Nome</label>
        <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome do novo plano">
    </div>
    <div class="form-group">
        <label for="velocidade">Velocidade</label>
        <input type="number" class="form-control" name="velocidade" id="velocidade" placeholder="Velocidade do novo plano">
    </div>
    <div class="form-group">
        <label for="descricao">Descrição</label>
        <textarea name="descricao" id="descricao" class="form-control" cols="10" rows="6" placeholder="Descrição do novo plano"></textarea>
    </div>
    <div class="form-group">
        <label for="preco">Preço</label>
        <input type="text" class="form-control" name="preco" id="preco" placeholder="Preço do novo plano">
    </div>
    <div class="form-group">
        <label for="preco">Status</label>
        <select class="form-control" name="status" id="status">
            <option value="Ativo">Ativo</option>
            <option value="Inativo">Inativo</option>
        </select>
    </div>
@endsection
<!--Javascript Adicionar Plano-->
@section('nomeNovoItem', 'novoPlano')
@section('conteudoNovoItem')
    nome: $('#nome').val(),
    velocidade: $('#velocidade').val(),
    descricao: $('#descricao').val(),
    preco: $('#preco').val(),
    status: $('#status').val()
@endsection
<!--Javascript Atualizar Etapa-->
@section('atualizarTabela')
    '<tr>' +
        '<td hidden>' + novoItem.id + '</td>' +
        '<td>' + novoItem.nome + '</td>' +
        '<td>' + novoItem.velocidade + '</td>' +
        '<td>' + novoItem.descricao + '</td>' +
        '<td>' + novoItem.preco + '</td>' +
        '<td>' + novoItem.status + '</td>' +
        '<td class="text-center">' + 
            '<button type="button" class="btn btn-primary mr-1" onclick="editar('+ novoItem.id +')">Editar</button>' +
            '<button type="button" class="btn btn-danger" onclick="modalExcluir('+ novoItem.id +')">Excluir</button>' +
        '</td>' +
    '</tr>'
@endsection
<!--Javascript Zerar inputs-->
@section('zerarInputs')
    $('#id').val(''),
    $('#nome').val(''),
    $('#velocidade').val(''),
    $('#descricao').val(''),
    $('#preco').val(''),
    $('#status').val('')
@endsection
<!--Javascript Editar Etapa-->
@section('carregarDadosEditar')
    $('#id').val(data.id),
    $('#nome').val(data.nome),
    $('#velocidade').val(data.velocidade),
    $('#descricao').val(data.descricao),
    $('#preco').val(data.preco),
    $('#status').val(data.status),
    $('#modalAdd').modal('show')
@endsection
@section('linhaAtualizar')
    linhaAtualizar[0].cells[1].textContent = atualizarItem.nome;
    linhaAtualizar[0].cells[2].textContent = atualizarItem.velocidade;
    linhaAtualizar[0].cells[3].textContent = atualizarItem.descricao;
    linhaAtualizar[0].cells[4].textContent = atualizarItem.preco;
    linhaAtualizar[0].cells[5].textContent = atualizarItem.status;
@endsection

@section('js')
    <script>
        $(function () {
            $('#preco').inputmask('R$9{1,10}');

            $('#formularioAdd').validate({
                errorElement: "em",
                validClass: "success",
                errorClass: "invalid text-danger small border-danger",
                rules: {
                    nome: {
                        required: true  
                    },
                    descricao: {
                        required: true  
                    },
                    velocidade: {
                        required: true  
                    },
                    preco: {
                        required: true  
                    },
                    status: {
                        required: true  
                    }
                },
                messages: {
                    nome: {
                        required: "Nome é obrigatório"
                    },
                    descricao: {
                        required: "Descrição é obrigatório"  
                    },
                    velocidade: {
                        required: "Velocidade é obrigatório"  
                    },
                    preco: {
                        required: "Preço é obrigatório"    
                    },
                    status: {
                        required: "Status do plano é obrigatório"    
                    }
                }
            })
        });
    </script>
@endsection