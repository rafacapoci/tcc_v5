<?php

use Illuminate\Support\Facades\Route; 

Route::resource('/atendimentos', 'App\Http\Controllers\AtendimentoController');
Route::resource('/tiposOcorrencias', 'App\Http\Controllers\TipoOcorrenciaController');
Route::resource('/etapas', 'App\Http\Controllers\EtapaController');
Route::resource('/setores', 'App\Http\Controllers\SetorController');
Route::resource('/cargos', 'App\Http\Controllers\CargoController');
Route::resource('/planos', 'App\Http\Controllers\PlanoController');
Route::resource('/pessoas', 'App\Http\Controllers\PessoaController');
Route::resource('/tiposClientes', 'App\Http\Controllers\TipoClienteController');
Route::resource('/clientes', 'App\Http\Controllers\ClienteController');
Route::resource('/contratos', 'App\Http\Controllers\ContratoController');
Route::resource('/funcionarios', 'App\Http\Controllers\FuncionarioController');