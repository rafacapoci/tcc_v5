<?php

use App\Http\Controllers\PlanoController;
use App\Http\Controllers\CargoController;
use App\Http\Controllers\SetorController;
use App\Http\Controllers\EtapaController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\FuncionarioController;
use App\Http\Controllers\TipoOcorrenciaController;
use App\Http\Controllers\AtendimentoController;
use App\Http\Controllers\ContratoController;
use App\Models\Etapa_Has_AtendimentoOcorrencia;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware('auth')->group(function () {
    //Pagina inicial - Atendimentos
    Route::get('/home', [AtendimentoController::class, 'indexView'])->name('home');
    Route::get('/atendimentos/{tipo?}/{conteudoPesquisa?}', [AtendimentoController::class, 'index']);

    //Planos
    Route::get('/planos', [PlanoController::class, 'indexView']);
    Route::get('/planos/{tipo?}/{conteudoPesquisa?}', [PlanoController::class, 'index']);

    //Cargos
    Route::get('/cargos', [CargoController::class, 'indexView']);
    Route::get('/cargos/{tipo?}/{conteudoPesquisa?}', [CargoController::class, 'index']);


    //Setores
    Route::get('/setores', [SetorController::class, 'indexView']);
    Route::get('/setores/{tipo?}/{conteudoPesquisa?}', [SetorController::class, 'index']);

    //Etapas
    Route::get('/etapas', [EtapaController::class, 'indexView']);
    Route::get('/etapas/{tipo?}/{conteudoPesquisa?}', [EtapaController::class, 'index']);

    //TiposOcorrencias
    Route::get('/tiposOcorrencias', [TipoOcorrenciaController::class, 'indexView']);
    Route::get('/tiposOcorrencias/{tipo?}/{conteudoPesquisa?}', [TipoOcorrenciaController::class, 'index']);

    //Clientes
    Route::get('/clientes', [ClienteController::class, 'indexView']);
    Route::get('/clientes/{tipo?}/{conteudoPesquisa?}', [ClienteController::class, 'index']);

    //Contratos
    Route::get('/contratos', [ContratoController::class, 'indexView']);
    Route::get('/contratos/{tipo?}/{conteudoPesquisa?}', [ContratoController::class, 'index']);

    //Funcionarios
    Route::get('/funcionarios', [FuncionarioController::class, 'indexView']);
    Route::get('/funcionarios/{tipo?}/{conteudoPesquisa?}', [FuncionarioController::class, 'index']);

});

